# System Lotto Notification

This project about call api website lottory or scraping content website lottory result and send line notification message to line group every 15 minutes or every 5 mintes. Admin page for management line group or view group near expiredate, count group avaliable in dashboard. In future we will develop feature about predictive lottory result from data.

## Scraping Web

https://www.ltobet.com/

https://www.lottovip.com/

https://www.huay.com/

https://www.ruay.com/

https://www.chudjenbet.com/

## Credits

https://www.linkedin.com/in/pattapees/

## Flow diagrams

System call api from website_lotto before send alert notify message to line group.

```mermaid
sequenceDiagram
system ->> website_lotto : [req] - datetime
website_lotto -->> system : [res] - lottory results
system -->> line_group: [res] - msg  lottory results
```
