import { EntityRepository, MoreThanOrEqual, Repository } from "typeorm";
import PredictiveLotto from "../entities/PredictiveLotto";

@EntityRepository(PredictiveLotto)
export class PredictiveLottoRepository extends Repository<PredictiveLotto> {
  public async Save(
    predictivelotto: PredictiveLotto
  ): Promise<PredictiveLotto> {
    await this.save(predictivelotto);

    return predictivelotto;
  }

  public async Update(
    id: string,
    predictivelotto: PredictiveLotto
  ): Promise<PredictiveLotto> {
    await this.manager.update(PredictiveLotto, id, predictivelotto);

    return predictivelotto;
  }

  public async getAll(): Promise<PredictiveLotto[]> {
    const result = await this.find({
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByRoundAndType(
    round: number,
    name: string
  ): Promise<PredictiveLotto[]> {
    const result = await this.find({
      where: { round, name },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByDate(
    name: string,
    length: number
  ): Promise<PredictiveLotto[]> {
    const result = await this.find({
      where: {
        name,
      },
      order: { createdAt: "DESC" },
      take: length,
    });

    return result;
  }
}
