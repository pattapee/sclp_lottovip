import { EntityRepository, Repository } from "typeorm";
import User from "../entities/User";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public async Save(user: User): Promise<User> {
    await this.save(user);

    return user;
  }

  public async getAll(): Promise<User[]> {
    const result = await this.find({
      where: { isActive: true },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getCount(): Promise<number> {
    const result = await this.count();

    return result;
  }

  public async getOneByID(id: string): Promise<User> {
    const result = await this.findOne({
      where: { id },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getOneByusername(username: string): Promise<User> {
    const result = await this.findOne({
      where: { username },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  // fn delete User
  public async Delete(user: User): Promise<User> {
    return await this.remove(user);
  }

  // fn update User
  public async Update(id: string, user: User): Promise<User> {
    await this.manager.update(User, id, user);

    return user;
  }
}
export default UserRepository;
