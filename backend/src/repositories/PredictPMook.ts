import { EntityRepository, LessThan, MoreThanOrEqual, Repository } from "typeorm";
import PredictPMook from "../entities/PredictPMook";

@EntityRepository(PredictPMook)
export class PredictPMookRepository extends Repository<PredictPMook> {
  public async Save(result: PredictPMook): Promise<PredictPMook> {
    await this.save(result);

    return result;
  }

  public async Update(
    id: string,
    predictivelotto: PredictPMook
  ): Promise<PredictPMook> {
    await this.manager.update(PredictPMook, id, predictivelotto);

    return predictivelotto;
  }

  public async Delete(result: PredictPMook): Promise<PredictPMook> {
    return await this.remove(result);
  }

  public async getOneByID(id: string): Promise<PredictPMook> {
    const result = await this.findOne({
      where: { id },
    });

    return result;
  }

  public async getAll(): Promise<PredictPMook[]> {
    const result = await this.find();

    return result;
  }

  public async getAllByActived(): Promise<PredictPMook[]> {
    const lotto = await this.find({
      where: { isActived: true, endDate: MoreThanOrEqual(new Date()) },
    });

    return lotto;
  }

  public async getAllByLessThanendDate(): Promise<PredictPMook[]> {
    const lotto = await this.find({
      where: { isActived: true, endDate: LessThan(new Date()) },
    });

    return lotto;
  }

  public async getAllIsActivedBywebOwner(
    webOwner: string
  ): Promise<PredictPMook[]> {
    const result = await this.find({
      where: {
        isActived: true,
        endDate: MoreThanOrEqual(new Date()),
        webOwner,
      },
    });

    return result;
  }
}
