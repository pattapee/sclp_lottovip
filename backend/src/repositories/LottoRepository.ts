import {
  EntityRepository,
  IsNull,
  LessThan,
  MoreThanOrEqual,
  Not,
  Repository,
} from "typeorm";
import Lotto from "../entities/Lotto";

@EntityRepository(Lotto)
export class LottoRepository extends Repository<Lotto> {
  public async Save(lotto: Lotto): Promise<Lotto> {
    await this.save(lotto);

    return lotto;
  }

  public async Delete(lotto: Lotto): Promise<Lotto> {
    return await this.remove(lotto);
  }

  public async Update(id: string, lotto: Lotto): Promise<Lotto> {
    await this.manager.update(Lotto, id, lotto);

    return lotto;
  }

  public async getAll(): Promise<Lotto[]> {
    const result = await this.find({
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByCategoryAndIsActive(
    category: string,
    isActive: boolean
  ): Promise<Lotto[]> {
    const result = await this.find({
      where: { category, isActive },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByIsActive(isActive: boolean): Promise<Lotto[]> {
    const result = await this.find({
      where: { isActive },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByMoreThanendDate(
    endDate: Date,
    category: string
  ): Promise<Lotto[]> {
    const result = await this.find({
      where: {
        category,
        isActive: true,
        endDate: MoreThanOrEqual(endDate),
      },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByMoreThanendDate2(endDate: Date): Promise<Lotto[]> {
    const result = await this.find({
      where: {
        isActive: true,
        endDate: MoreThanOrEqual(endDate),
      },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByMoreThanendDatePredictive(
    endDate: Date,
    category: string
  ): Promise<Lotto[]> {
    const result = await this.find({
      where: {
        category,
        isActive: true,
        endDate: MoreThanOrEqual(endDate),
        isPredictived: true,
      },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByLessThanendDate(
    endDate: Date,
    category: string
  ): Promise<Lotto[]> {
    const result = await this.find({
      where: {
        category,
        isActive: true,
        endDate: LessThan(endDate),
      },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByLessThanendDate2(endDate: Date): Promise<Lotto[]> {
    const result = await this.find({
      where: {
        isActive: true,
        endDate: LessThan(endDate),
      },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getOneByID(id: string): Promise<Lotto> {
    const result = await this.findOne({
      where: { id },
    });

    return result;
  }
}
export default LottoRepository;
