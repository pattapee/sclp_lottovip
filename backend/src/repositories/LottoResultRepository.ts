import { EntityRepository, MoreThanOrEqual, Repository } from "typeorm";
import LottoResult from "../entities/LottoResult";

@EntityRepository(LottoResult)
export class LottoResultRepository extends Repository<LottoResult> {
  public async Save(lottoresult: LottoResult): Promise<LottoResult> {
    await this.save(lottoresult);

    return lottoresult;
  }

  public async getByLastCreatedAt(
    round: number,
    name: string
  ): Promise<LottoResult> {
    const result = await this.findOne({
      where: { round, name },
      order: { createdAt: "DESC" },
    });

    return result;
  }

  public async Update(
    id: string,
    lottoresult: LottoResult
  ): Promise<LottoResult> {
    await this.manager.update(LottoResult, id, lottoresult);

    return lottoresult;
  }

  public async getAll(): Promise<LottoResult[]> {
    const result = await this.find({
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByRoundAndType(
    round: number,
    name: string
  ): Promise<LottoResult[]> {
    const result = await this.find({
      where: { round, name },
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getAllByDate(name: string, length: number): Promise<LottoResult[]> {
    const result = await this.find({
      where: {
        name,
      },
      order: { createdAt: "DESC" },
      take: length,
    });

    return result;
  }
}
export default LottoResultRepository;
