import { EntityRepository, MoreThanOrEqual, Repository } from "typeorm";
import LottoRound from "../entities/LottoRound";

@EntityRepository(LottoRound)
export class LottoRoundRepository extends Repository<LottoRound> {
  public async Save(lottoround: LottoRound): Promise<LottoRound> {
    await this.save(lottoround);

    return lottoround;
  }

  public async Update(id: string, lottoround: LottoRound): Promise<LottoRound> {
    await this.manager.update(LottoRound, id, lottoround);

    return lottoround;
  }

  public async getAll(): Promise<LottoRound[]> {
    const result = await this.find({
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  public async getOne(): Promise<LottoRound> {
    const result = await this.findOne({
      order: { updatedAt: "DESC" },
    });

    return result;
  }

  //   public async getAllByRoundAndType(
  //     round: number,
  //     name: string
  //   ): Promise<LottoResult[]> {
  //     const result = await this.find({
  //       where: { round, name },
  //       order: { updatedAt: "DESC" },
  //     });

  //     return result;
  //   }

  //   public async getAllByDate(name: string, length: number): Promise<LottoResult[]> {
  //     const result = await this.find({
  //       where: {
  //         name,
  //       },
  //       order: { createdAt: "DESC" },
  //       take: length,
  //     });

  //     return result;
  //   }
}
export default LottoRoundRepository;
