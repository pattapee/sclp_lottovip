import { Request, Response, Router } from "express";
import { HTTPSTATUS_OK } from "../constants/HttpStatus";
import { checkJwt } from "../middlewares/checkJwt";
// import { checkRole } fro../services/LineNotify/LottoVIP
import Auithentication from "../services/Auithentication";
import Dashboard from "../services/Dashboard";
import Lotto from "../services/Lotto";

import UtilLotto from "../services/LineNotify/util";
import PredictivePMook from "../services/predictpmook";
import UserServices from "../services/User";

const routes = Router();

// CheckHealth HW
routes.get("/checkhealth", async (req: Request, res: Response) => {
  res.status(HTTPSTATUS_OK).send({ data: "Avaliable" });
});

// Services Users
routes.post("/users", [checkJwt], UserServices.savePerson);
routes.patch("/users", [checkJwt], UserServices.updatePerson);
routes.delete("/users/:id", [checkJwt], UserServices.deletePerson);
routes.get("/users", [checkJwt], UserServices.getAllPerson);
routes.get("/users/:id", [checkJwt], UserServices.getOnePersonByID);
routes.patch("/users/reset/:id", [checkJwt], UserServices.resetPassword);

// Services Authentication
routes.post("/login", Auithentication.login);
routes.post("/checktoken", Auithentication.checkToken);

// Services Lotto
routes.post("/lotto", [checkJwt], Lotto.saveLotto);
routes.patch("/lotto", [checkJwt], Lotto.updateLotto);
routes.patch("/lotto/active", [checkJwt], Lotto.updateActived);
routes.delete("/lotto/:id", [checkJwt], Lotto.delete);
routes.get("/lotto/list/:category/:isactive", [checkJwt], Lotto.getAll);
routes.get("/lotto/:id", [checkJwt], Lotto.getOneByID);

// Services Report and Dashboard
routes.get("/dashboard", [checkJwt], Dashboard.getDataDashboard);
routes.get("/count", [checkJwt], Dashboard.getCountLotto);
routes.get("/round", [checkJwt], Dashboard.getRoundLotto);
routes.get("/lotto/trigger/:category", [checkJwt], UtilLotto.triggerLineNotify);

// Services Predictive P'mook
routes.post("/predictpmook", [checkJwt], PredictivePMook.save);
routes.patch("/predictpmook", [checkJwt], PredictivePMook.update);
routes.get("/predictpmook", [checkJwt], PredictivePMook.getAll);
routes.get("/predictpmook/:id", [checkJwt], PredictivePMook.getOneByID);
routes.delete("/predictpmook/:id", [checkJwt], PredictivePMook.delete);
routes.patch(
  "/predictpmook/status",
  [checkJwt],
  PredictivePMook.updateisActived
);
routes.post(
  "/predictpmook/autocreategroupak1",
  [checkJwt],
  PredictivePMook.autoCreateGroupPak1
);

export default routes;
