export default function numberIncrease(value: number, length: number) {
  const numberPredict = [];
  for (let i = 0; i < length; i++) {
    const number = Number(value) + i;

    numberPredict.push(number < 10 ? number : number.toString().split("")[1]);
  }

  return numberPredict.toString().replace(/,/g, "");
}
