import {
  HTTPSTATUS_BADREQUEST,
  HTTPSTATUS_CONFLICT,
} from "../../constants/HttpStatus";
import logger from "../Logging";

export default function handleResponseError(err: any) {
  logger.error(err);
  if (err.details) {
    return {
      status: HTTPSTATUS_CONFLICT,
      data: "Failed to validate input " + err.details[0].message,
    };
  } else {
    return { status: HTTPSTATUS_BADREQUEST, data: err };
  }
}
