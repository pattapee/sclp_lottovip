import { slice } from "lodash";
import forEach from "lodash/forEach";

import { TypeLottoReport } from "../LineNotify/type";
import tranformResultLotto from "./tranformResultLotto";

export default function handleMsgTxtLotto(
  nameGroup: string,
  nameReport: string,
  objDataLotto: TypeLottoReport[]
) {
  let message = nameGroup;
  message += `➖➖➖➖➖➖➖\n`;
  const lengthLotto = objDataLotto.length;

  let resultObj: TypeLottoReport[] = [];
  if (lengthLotto > 15) {
    resultObj = slice(
      objDataLotto,
      objDataLotto.length - 15,
      objDataLotto.length
    );
  } else {
    resultObj = objDataLotto;
  }

  forEach(resultObj, (value: TypeLottoReport) => {
    const { result3, result2 } = tranformResultLotto(value.data3, value.data2);
    message += `${value.number}: ${value.time} ➡️ ${result3} - ${result2}\n`;
  });

  message += `➖➖➖➖➖➖➖➖\n`;
  message += `รายงานผล ${nameReport} ⚠️ \n`;
  message += `ผล ${nameReport} รอบที่ ${
    resultObj[resultObj.length - 1].number
  }  \n`;

  const lastDataLotto = resultObj[resultObj.length - 1];
  const { result3: lastResult3, result2: lastResult2 } = tranformResultLotto(
    lastDataLotto.data3,
    lastDataLotto.data2
  );

  message += `         ${lastResult3} - ${lastResult2} \n`;
  message += `➖➖➖➖➖➖➖\n`;

  return { message, resultObj };
}
