export default function tranformResultLotto(lotto3: number, lotto2: number) {
  let result3 = `000${lotto3}`;
  result3 = result3.substring(result3.length - 3);
  let result2 = `00${lotto2}`;
  result2 = result2.substring(result2.length - 2);

  return { result3, result2 };
}
