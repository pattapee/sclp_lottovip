import dotenv from "dotenv";
import { Request, Response } from "express";
import Joi from "joi";
import * as _ from "lodash";
import { getConnection } from "typeorm";

import {
  HTTPSTATUS_ACCEPT,
  HTTPSTATUS_BADREQUEST,
  HTTPSTATUS_CONFLICT,
  HTTPSTATUS_CREATE,
  HTTPSTATUS_OK,
} from "../constants/HttpStatus";
import { User } from "../entities/User";
import { UserRepository } from "../repositories/UserRepository";
import handleResponseError from "./Util/handleResponseError";

dotenv.config();

let repository: UserRepository;
const initialize = () => {
  repository = getConnection().getCustomRepository(UserRepository);
};

export default class UserServices {
  public static getAllPerson = async (req: Request, res: Response) => {
    try {
      await initialize();
      const result = await repository.getAll();

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getOnePersonByID = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initialize();
      const result = await repository.getOneByID(value.id);
      if (result) {
        return res.status(HTTPSTATUS_OK).send(result);
      } else {
        return res.status(HTTPSTATUS_BADREQUEST).send({});
      }
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static savePerson = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      username: Joi.string().required(),
      password: Joi.string().required(),
      fullname: Joi.string().required(),
      role: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      await initialize();
      const chkDuplicateUsername = await repository.getOneByusername(
        req.body.username
      );

      if (chkDuplicateUsername) {
        return res
          .status(HTTPSTATUS_CONFLICT)
          .send({ data: "Duplicate username !!" });
      } else {
        const user = new User();
        user.username = value.username;
        user.password = value.password;
        user.fullname = value.fullname;
        user.role = value.role;
        user.hashPassword();
        const result = await repository.Save(user);

        return res.status(HTTPSTATUS_CREATE).send(result);
      }
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static updatePerson = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
      fullname: Joi.string().required(),
      role: Joi.string().allow(""),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const user = new User();
      user.fullname = value.fullname;
      user.role = value.role;
      await initialize();
      const result = await repository.Update(value.id, user);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static deletePerson = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initialize();
      const user = await repository.getOneByID(value.id);
      user.isActive = false;

      const result = await repository.update(user.id, user);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static resetPassword = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initialize();
      const user = await repository.getOneByID(value.id);
      user.password = process.env.defaultPasswordUser;
      user.hashPassword();

      const result = await repository.update(user.id, user);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }
}
