import { Request, Response } from "express";
import { countBy, orderBy } from "lodash";
import moment from "moment";
import { getConnection } from "typeorm";

import { HTTPSTATUS_OK } from "../constants/HttpStatus";
import { LottoRepository } from "../repositories/LottoRepository";
import { LottoResultRepository } from "../repositories/LottoResultRepository";
import { LottoRoundRepository } from "../repositories/LottoRoundRepository";
import { PredictiveLottoRepository } from "../repositories/PredictiveLottoRepository";
import handleResponseError from "./Util/handleResponseError";

let lottorepository: LottoRepository;
let lottoresultrepository: LottoResultRepository;
let predictivelottorepository: PredictiveLottoRepository;
let lottoroundrepository: LottoRoundRepository;

const initialize = () => {
  lottorepository = getConnection().getCustomRepository(LottoRepository);
  lottoresultrepository = getConnection().getCustomRepository(
    LottoResultRepository
  );
  predictivelottorepository = getConnection().getCustomRepository(
    PredictiveLottoRepository
  );
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class DashboardController {
  public static getDataDashboard = async (req: Request, res: Response) => {
    try {
      await initialize();
      const lottoround = await lottoroundrepository.getOne();
      const lotto = await lottorepository.getAllByIsActive(true);
      const lengthHuay = lottoround.huay;
      const lengthHuaySpeed = lottoround.huayspeed;
      const lengthLottoVIP = lottoround.lottovip;
      const lengthLtobet = lottoround.ltobet;
      const lengthRuay = lottoround.ruay;
      const lengthDNABet = lottoround.dnabetspeed;

      const dataListNearExpiry = lotto.filter((value) => {
        const resultDatediff = moment(value.endDate).diff(
          moment(new Date()),
          "days"
        );
        return resultDatediff <= 30;
      });
      const lottoResult = await lottoresultrepository.getAllByDate(
        "lottovip",
        +lengthLottoVIP > 0 ? +lengthLottoVIP - 1 : 1
      );

      const predictivelottoResult =
        await predictivelottorepository.getAllByDate(
          "lottovip",
          +lengthLottoVIP > 0 ? +lengthLottoVIP - 1 : 1
        );

      const dataDashboard = {
        oldlengthLottoVIP: lengthLottoVIP,
        oldlengthRuay: lengthRuay,
        oldlengthHuay: lengthHuay,
        oldlengthHuaySpeed: lengthHuaySpeed,
        oldlengthLtobet: lengthLtobet,
        oldlengthDNABet: lengthDNABet,
        countActive: countBy(lotto, "category"),
        updatedAt: new Date(),
        dataListNearExpiry: orderBy(dataListNearExpiry, ["endDate"], ["asc"]),
        lottoResult,
        predictivelottoResult,
      };

      return res.status(HTTPSTATUS_OK).send(dataDashboard);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getCountLotto = async (req: Request, res: Response) => {
    try {
      await initialize();
      const lottoActivate = await lottorepository.getAllByIsActive(true);
      const lottoInActivate = await lottorepository.getAllByIsActive(false);

      const dataDashboard = {
        countActive: countBy(lottoActivate, "category"),
        countInActive: countBy(lottoInActivate, "category"),
        updatedAt: new Date(),
      };

      return res.status(HTTPSTATUS_OK).send(dataDashboard);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getRoundLotto = async (req: Request, res: Response) => {
    try {
      await initialize();
      const lottoround = await lottoroundrepository.getOne();

      const roundLotto = {
        oldlengthLottoVIP: lottoround.lottovip,
        oldlengthRuay: lottoround.ruay,
        oldlengthHuay: lottoround.huay,
        oldlengthHuaySpeed: lottoround.huayspeed,
        oldlengthLtobet: lottoround.ltobet,
        oldlengthChudjenbet: lottoround.chudjenbet,
        oldlengthChudjenbetSpeed: lottoround.chudjenbetspeed,
        oldlengthDNABet: lottoround.dnabet,
        oldlengthDNABetSpeed: lottoround.dnabetspeed,
        updatedAt: new Date(),
      };
      return res.status(HTTPSTATUS_OK).send(roundLotto);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }
}

export default DashboardController;
