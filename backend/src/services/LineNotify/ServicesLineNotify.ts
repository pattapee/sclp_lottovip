import get from "lodash/get";
import orderBy from "lodash/orderBy";
import moment from "moment";
import { getConnection } from "typeorm";

import Lotto from "../../entities/Lotto";
import LottoRound from "../../entities/LottoRound";
import PredictPMook from "../../entities/PredictPMook";
import { LottoRepository } from "../../repositories/LottoRepository";
import { LottoResultRepository } from "../../repositories/LottoResultRepository";
import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
import { PredictPMookRepository } from "../../repositories/PredictPMook";
import logger from "../Logging";

let lottorepository: LottoRepository;
const initializelotto = () => {
  lottorepository = getConnection().getCustomRepository(LottoRepository);
};
let lottoresultrepository: LottoResultRepository;
const initializelottoresult = () => {
  lottoresultrepository = getConnection().getCustomRepository(
    LottoResultRepository
  );
};
let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};
let predictpmookrepository: PredictPMookRepository;
const initializepredictpmook = () => {
  predictpmookrepository = getConnection().getCustomRepository(
    PredictPMookRepository
  );
};
interface TypeLottoExpire {
  id: string;
  linetoken: string;
  endDate: string;
}
export class ServicesLineNotify {
  public static APISendLinenotify = async (message: string, token: string) => {
    const Notify_SDK = require("line-notify-sdk");
    const sdk = new Notify_SDK();

    await sdk.notify(token, message).catch((e: any) => {
      logger.error(e);
    });
  }

  public static SendLineNotify = async (message: string) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "lottovip"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = message;
      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyRuay = async (message: string) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "ruay"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = message;
      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyHuay = async (
    huayMessage: string,
    huayVIPMessage: string
  ) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "huay"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = "";

      const huay = get(value, "huay", false);
      const huayvip = get(value, "huayvip", false);
      const huayspeed = get(value, "huayspeed", false);
      const huayspeedvip = get(value, "huayspeedvip", false);

      if (huay && huayvip) {
        messageEditor = huayMessage + huayVIPMessage;
      } else if (huay && !huayvip) {
        messageEditor = huayMessage;
      } else if (!huay && huayvip) {
        messageEditor = huayVIPMessage;
      }

      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor && !huayspeed && !huayspeedvip) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyHuaySpeed = async (
    huayMessage: string,
    huayVIPMessage: string
  ) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "huay"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = "";
      const huay = get(value, "huay", false);
      const huayvip = get(value, "huayvip", false);
      const huayspeed = get(value, "huayspeed", false);
      const huayspeedvip = get(value, "huayspeedvip", false);

      if (huayspeed && huayspeedvip) {
        messageEditor = huayMessage + huayVIPMessage;
      } else if (huayspeed && !huayspeedvip) {
        messageEditor = huayMessage;
      } else if (!huayspeed && huayspeedvip) {
        messageEditor = huayVIPMessage;
      }

      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor && !huay && !huayvip) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyLtobetYeekee = async (
    SumMessage: string,
    LtobetMessage: string,
    LtobetVIPMessage: string
  ) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "ltobet"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = "";
      const ltobet_yeekee = get(value, "ltobetyeekee", false);

      const ltobet_yeekee_vip = get(value, "ltobetyeekeevip", false);

      if (ltobet_yeekee && ltobet_yeekee_vip) {
        messageEditor = SumMessage;
      } else if (ltobet_yeekee && !ltobet_yeekee_vip) {
        messageEditor = LtobetMessage;
      } else if (!ltobet_yeekee && ltobet_yeekee_vip) {
        messageEditor = LtobetVIPMessage;
      }

      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyChudjenbet = async (
    Message: string,
    MessageVIP: string
  ) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "chudjenbet"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = "";
      const chudjenbet = get(value, "chudjenbet", false);
      const chudjenbetvip = get(value, "chudjenbetvip", false);

      if (chudjenbet && chudjenbetvip) {
        messageEditor = Message + MessageVIP;
      } else if (chudjenbet && !chudjenbetvip) {
        messageEditor = Message;
      } else if (!chudjenbet && chudjenbetvip) {
        messageEditor = MessageVIP;
      }

      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static SendLineNotifyDNABetSpeed = async (
    Message: string,
    MessageVIP: string
  ) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "dnabet"
    );

    lottoToken.forEach(async (value) => {
      let messageEditor = "";
      const data = value.dnabetspeed || false;
      const datavip = get(value, "dnabetspeedvip", false);

      if (data && datavip) {
        messageEditor = Message + MessageVIP;
      } else if (data && !datavip) {
        messageEditor = Message;
      } else if (!data && datavip) {
        messageEditor = MessageVIP;
      }

      messageEditor += value.remark ? `${value.remark}\n` : null;

      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static startalert0600 = async () => {
    await initializelottoround();
    const lottoround = new LottoRound();
    lottoround.lottovip = 0;
    lottoround.huay = 0;
    lottoround.huayspeed = 0;
    lottoround.ltobet = 0;
    lottoround.ltobetspeed = 0;
    lottoround.ruay = 0;
    lottoround.chudjenbet = 0;
    lottoround.chudjenbetspeed = 0;
    lottoround.dnabet = 0;
    lottoround.dnabetspeed = 0;
    await lottoroundrepository.update(lottoround.id, lottoround);
    await ServicesLineNotify.sendMsgCountDown();
    await ServicesLineNotify.scheduleCheckendDateToken();
  }

  public static scheduleCheckendDateToken = async () => {
    await initializelotto();
    await initializepredictpmook();

    try {
      const lotto = await lottorepository.getAllByLessThanendDate2(new Date());

      lotto.forEach(async (value: Lotto) => {
        value.isActive = false;

        await lottorepository.update(value.id, value);
      });

      const listPredictPmook =
        await predictpmookrepository.getAllByLessThanendDate();

      listPredictPmook.forEach(async (value: PredictPMook) => {
        value.isActived = false;

        await predictpmookrepository.update(value.id, value);
      });

      // listPredictPmook.forEach(async (value) => {
      // const message = `\nระบบของท่านจะหมดอายุในวันที่ ${moment(value.endDate)
      //   .add(543, "year")
      //   .format("DD/MM/YYYY")}`;

      // await ServicesLineNotify.APISendLinenotify(message, value.linetoken);
      // });

      // const huay = await lottorepository.getAllByLessThanendDate(
      //   new Date(),
      //   "huay"
      // );

      // huay.forEach(async (value: Lotto) => {
      //   value.isActive = false;

      //   await lottorepository.update(value.id, value);
      // });

      // const ltobet = await lottorepository.getAllByLessThanendDate(
      //   new Date(),
      //   "ltobet"
      // );

      // ltobet.forEach(async (value: Lotto) => {
      //   value.isActive = false;

      //   await lottorepository.update(value.id, value);
      // });
    } catch (err) {
      logger.error(err);
    }
  }

  public static sendMsgCountDown = async () => {
    const message = `\nหมดเวลา แทงบนเว็บ\nรอผลออกอีก 3 นาที`;

    await ServicesLineNotify.SendMsgCountdown3min(message);
  }

  public static sendMsgCountDownForSpeed = async () => {
    const message = `\nหมดเวลา แทงบนเว็บ\nรอผลออกอีก 3 นาที`;

    await ServicesLineNotify.SendMsgCountdown3minForSpeed(message);
  }

  public static sendMsgStartnotify = async () => {
    await initializelotto();
    await initializepredictpmook();

    const lottoToken = await lottorepository.getAllByMoreThanendDate2(
      new Date()
    );

    lottoToken.forEach(async (value) => {
      const message = `\nระบบของท่านจะหมดอายุในวันที่ ${moment(value.endDate)
        .add(543, "year")
        .format("DD/MM/YYYY")}`;

      await ServicesLineNotify.APISendLinenotify(message, value.linetoken);
    });

    const listPredictPmook = await predictpmookrepository.getAllByActived();

    listPredictPmook.forEach(async (value) => {
      const message = `\nระบบของท่านจะหมดอายุในวันที่ ${moment(value.endDate)
        .add(543, "year")
        .format("DD/MM/YYYY")}`;

      await ServicesLineNotify.APISendLinenotify(message, value.linetoken);
    });
  }

  public static sendAlertDeadline = async () => {
    await initializelotto();

    const sendAlertExpireDate = async (
      resultDatediff: number,
      messageEditor: string,
      id: string,
      linetoken: string,
      endDate: Date
    ) => {
      if (resultDatediff >= 0 && resultDatediff <= 2) {
        messageEditor += `ระบบจะหมดอายุในวันที่ ${moment(endDate)
          .add(543, "year")
          .format("DD/MM/YYYY")} กรุณาติดต่อผู้ดูแลระบบโดยแจ้งรหัสชุดนี้ ${id}`;
      }
      if (messageEditor) {
        ServicesLineNotify.APISendLinenotify(messageEditor, linetoken);
      }
    };

    const lottoToken = await lottorepository.getAllByMoreThanendDate2(
      new Date()
    );

    lottoToken.forEach(async (value) => {
      const messageEditor = "";
      const resultDatediff = moment(value.endDate).diff(
        moment(new Date()),
        "days"
      );

      sendAlertExpireDate(
        resultDatediff,
        messageEditor,
        value.id,
        value.linetoken,
        value.endDate
      );
    });

    const listPredictPmook = await predictpmookrepository.getAllByActived();

    listPredictPmook.forEach(async (value) => {
      const messageEditor = "";
      const resultDatediff = moment(value.endDate).diff(
        moment(new Date()),
        "days"
      );

      sendAlertExpireDate(
        resultDatediff,
        messageEditor,
        value.id,
        value.linetoken,
        value.endDate
      );
    });
  }

  public static SendMsgCountdown3min = async (message: string) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate2(
      new Date()
    );

    lottoToken.forEach(async (value) => {
      const messageEditor = message;
      if (messageEditor) {
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });

    // const HuayToken = await lottorepository.getAllByMoreThanendDate(
    //   new Date(),
    //   "huay"
    // );

    // HuayToken.forEach(async (value) => {
    //   const messageEditor = message;
    //   await ServicesLineNotify.APISendLinenotify(
    //     messageEditor,
    //     value.linetoken
    //   );
    // });

    // const ltobetToken = await lottorepository.getAllByMoreThanendDate(
    //   new Date(),
    //   "ltobet"
    // );

    // ltobetToken.forEach(async (value) => {
    //   const messageEditor = message;
    //   await ServicesLineNotify.APISendLinenotify(
    //     messageEditor,
    //     value.linetoken
    //   );
    // });

    // const ruayToken = await lottorepository.getAllByMoreThanendDate(
    //   new Date(),
    //   "ruay"
    // );

    // ruayToken.forEach(async (value) => {
    //   const messageEditor = message;
    //   await ServicesLineNotify.APISendLinenotify(
    //     messageEditor,
    //     value.linetoken
    //   );
    // });
  }

  public static SendMsgCountdown3minForSpeed = async (message: string) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDate(
      new Date(),
      "huay"
    );

    lottoToken.forEach(async (value) => {
      const huayvip = get(value, "huayvip", false);

      if (huayvip) {
        const messageEditor = message;
        await ServicesLineNotify.APISendLinenotify(
          messageEditor,
          value.linetoken
        );
      }
    });
  }

  public static sendMsgPredictLottoVIP = async (message: string) => {
    await initializelotto();

    const lottoToken = await lottorepository.getAllByMoreThanendDatePredictive(
      new Date(),
      "lottovip"
    );

    lottoToken.forEach(async (value) => {
      const messageEditor = message;
      await ServicesLineNotify.APISendLinenotify(
        messageEditor,
        value.linetoken
      );
    });
  }

  public static predictiveData = async (oldlength: string, name: string) => {
    await initializelottoresult();

    try {
      const listLottoResult = await lottoresultrepository.getAllByRoundAndType(
        +oldlength,
        name
      );

      if (!listLottoResult) {
        return [];
      }

      const countNumber: number[] = [];

      listLottoResult.forEach((lottoresult) => {
        const lotto2cha = lottoresult.lotto2cha.toString().split("");

        const lotto3cha = lottoresult.lotto3cha.toString().split("");

        lotto2cha.forEach((value) => {
          countNumber.push(+value);
        });
        lotto3cha.forEach((value) => {
          countNumber.push(+value);
        });
      });

      const dataPredictive = [];
      for (let i = 0; i < 10; i++) {
        const length = countNumber.filter((number) => number === i).length;
        const result = {
          number: i,
          countNumber: length,
          persent: Math.round((length / countNumber.length) * 100),
        };
        dataPredictive.push(result);
      }

      const limitDataPredictive = orderBy(
        dataPredictive,
        ["persent"],
        ["desc"]
      ).slice(0, 2);

      return limitDataPredictive;
    } catch (err) {
      logger.error(err);
    }
  }
}

export default ServicesLineNotify;
