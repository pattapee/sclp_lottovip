import compact from "lodash/compact";
import moment from "moment";
import { getConnection } from "typeorm";
import LottoResult from "../../entities/LottoResult";
import { LottoResultRepository } from "../../repositories/LottoResultRepository";
import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
import logger from "../Logging";
import handleMsgTxtLotto from "../Util/handleMsgTxtLotto";
import { ScrapingWeb } from "./ScrapingWeb";
import { ServicesLineNotify } from "./ServicesLineNotify";
import { TypeLottoReport } from "./type";

let lottoresultrepository: LottoResultRepository;
const initializelottoresult = () => {
  lottoresultrepository = getConnection().getCustomRepository(
    LottoResultRepository
  );
};

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class APILineNotifyRuay {
  public static InitalOldlength = async () => {
    logger.info("Start InitalOldlength Ruay");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { ruay: 0 });
    } else {
      const getData = async () => {
        let Lotto: TypeLottoReport[] = [];
        await new Promise((resolve) => setTimeout(resolve, 5000));
        Lotto = compact(await ScrapingWeb.getDataLottoVIP());
        return Lotto;
      };

      const objDataRuayresult = await getData();

      if (objDataRuayresult) {
        await lottoroundrepository.update(lottoround.id, {
          ruay: objDataRuayresult.length,
        });
      }
    }
    logger.info("End InitalOldlength Ruay");
  }

  public static fnInital = async () => {
    logger.info("Start fnInital Ruay");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { ruay: 0 });
    } else {
      const getDataLotto = async () => {
        let Lotto: TypeLottoReport[] = [];
        do {
          await new Promise((resolve) => setTimeout(resolve, 5000));
          Lotto = compact(await ScrapingWeb.getDataRuay());
        } while (Lotto.length === lottoround.ruay || Lotto.length === 0);

        return Lotto;
      };

      const objDataLtobetresult = await getDataLotto();

      if (objDataLtobetresult) {
        const { message, resultObj } = await handleMsgTxtLotto(
          `\n        💰 Ruay 💰\n`,
          "Ruay",
          objDataLtobetresult
        );

        const lotto2cha = resultObj[resultObj.length - 1].data2;
        const lotto3cha = resultObj[resultObj.length - 1].data3;

        const result = {
          name: "Ruay",
          data: `${lotto3cha} - ${lotto2cha}`,
          round: objDataLtobetresult.length,
        };

        await ServicesLineNotify.SendLineNotifyRuay(message);

        await lottoroundrepository.update(lottoround.id, {
          ruay: result.round,
        });

        await initializelottoresult();
        const lottoresult = new LottoResult();
        lottoresult.lotto2cha = lotto2cha;
        lottoresult.lotto3cha = lotto3cha;
        lottoresult.name = "ruay";
        lottoresult.round = result.round;
        lottoresultrepository.Save(lottoresult);

        logger.info(result);
      }
    }
    logger.info("End fnInital Ruay");
  }
}
export default APILineNotifyRuay;
