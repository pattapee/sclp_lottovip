import axios from "axios";
import cheerio from "cheerio";
import https from "https";
import find from "lodash/find";
import isEmpty from "lodash/isEmpty";
import omitBy from "lodash/omitBy";
import moment from "moment";

import logger from "../Logging";
import { Ltobet, TypeHuay, TypeLottoReport } from "./type";

const AxiosInstance = axios.create({
  timeout: 50000,
  withCredentials: true,
  httpsAgent: new https.Agent({ keepAlive: true }),
  headers: { "Content-Type": "application/json" },
});

const splitResultLottoVIP = async (html: string) => {
  let count = 0;
  const obj = [];

  for (let i = 0; i < 88; ++i) {
    const data3 = Number(html.substring(count, count + 3));
    const data2 = Number(html.substring(count + 3, count + 5));

    obj.push({
      data3,
      data2,
      number: i + 1,
      time: moment("05:59:00", "HH:mm")
        .add(15 * i + 1, "m")
        .format("HH:mm"),
    });
    count += 5;
  }

  let x = 0;
  obj.forEach((value) => {
    if (value.data2 && value.data3) {
      x = value.number;
    }
  });

  return obj.slice(0, x);
};

const getAPIResultLotto = async (category: string) => {
  let currentDate = moment(new Date());

  currentDate =
    currentDate.hours() < 6 ? currentDate.add(-1, "days") : currentDate;
  const Humanoid = require("humanoid-js");
  const humanoid = new Humanoid();

  switch (category) {
    case "Ltobet":
      return await AxiosInstance.get(
        `https://www.ltobet.com/api/member/lotto/result/${currentDate.format(
          "YYYY-MM-DD"
        )}`
      );
    case "Huay":
      return await AxiosInstance.get(
        `https://www.huay.com/api/v1/public/lottery/list-round-result?round_date=${currentDate.format(
          "YYYY-MM-DD"
        )}`
      );
    case "Chudjenbet":
      return (
        await humanoid.get(
          `https://chudjenbet.com/api/member/lotto/result/${currentDate.format(
            "YYYY-MM-DD"
          )}`
        )
      ).body;
    case "DNABet":
      return (
        await humanoid.get(
          `https://www.dnabet.com/api/v1/public/lottery/list-round-result?round_date=${currentDate.format(
            "YYYY-MM-DD"
          )}`
        )
      ).body;
  }
};

const fnAddIndex = async (yeekee: TypeLottoReport[]) => {
  return yeekee.map((value: TypeLottoReport, index) => {
    value.number = index + 1;
    return value;
  });
};

export class ScrapingWeb {
  public static getDataLottoVIP = async () => {
    try {
      const html = await AxiosInstance.get("https://thelottovip.co/login");

      const $ = await cheerio.load(html.data);
      const statsTable = $("#yeekee").find(".card-text").text();
      const result = await splitResultLottoVIP(statsTable.substring(0, 440));

      return result;
    } catch (err) {
      logger.error(err);
      return [];
    }
  }

  public static getDataRuay = async () => {
    try {
      const html = await axios.get("https://www.ruay.com/login");
      const $ = await cheerio.load(html.data);
      const statsTable = $("#yeekee").find(".card-text").text();

      const result = await splitResultLottoVIP(statsTable.substring(0, 440));

      return result;
    } catch (err) {
      logger.error(err);
      return [];
    }
  }

  public static getDataHuayFromsubCategory = async (subCategory: string) => {
    try {
      let subCategoryCode;

      switch (subCategory) {
        case "huay":
          subCategoryCode = "0101";
        case "huayvip":
          subCategoryCode = "0103";
        case "huayspeed":
          subCategoryCode = "0102";
        case "huayspeedvip":
          subCategoryCode = "0104";
      }

      const dataLotto = await getAPIResultLotto("Huay");

      const dataHuay = Object.entries(
        omitBy(
          dataLotto.data.data[subCategoryCode],
          (data) => data.result == null
        )
      ).map((value: any) => {
        return value[1];
      });

      if (dataHuay) {
        return dataHuay.map((value: TypeHuay) => {
          return {
            data3: Number(value.result.top_three),
            data2: Number(value.result.bottom_two),
            number: Number(value.round_number),
            time: moment(value.end_at).format("HH:mm"),
          };
        });
      }

      return [];
    } catch (err) {
      logger.error(err);
      return [];
    }
  }

  public static getDataLtobet = async () => {
    try {
      const dataLotto = await getAPIResultLotto("Ltobet");

      const yeekee: Ltobet[] = [];
      const yeekee_vip: Ltobet[] = [];

      (dataLotto.data.categories || []).forEach((value: Ltobet) => {
        if (value.type === "yeekee") {
          value.result_lotto = find(dataLotto.data.records, {
            category_id: value.category.id,
          });

          if (!isEmpty(value.result_lotto.result)) {
            yeekee.push(value);
          }
        }

        if (value.type === "yeekee_vip") {
          value.result_lotto = find(dataLotto.data.records, {
            category_id: value.category.id,
          });

          if (!isEmpty(value.result_lotto.result)) {
            yeekee_vip.push(value);
          }
        }
      });

      if (yeekee && yeekee_vip) {
        return { yeekee, yeekee_vip };
      } else {
        return { yeekee: [] as Ltobet[], yeekee_vip: [] as Ltobet[] };
      }
    } catch (err) {
      logger.error(err);
      return { yeekee: [] as Ltobet[], yeekee_vip: [] as Ltobet[] };
    }
  }

  public static getDataLtobetSpeed = async () => {
    try {
      const dataLotto = await getAPIResultLotto("Ltobet");

      const speed: Ltobet[] = [];
      const speed_vip: Ltobet[] = [];

      (dataLotto.data.categories || []).forEach((value: Ltobet) => {
        if (value.type === "speed") {
          value.result_lotto = find(dataLotto.data.records, {
            category_id: value.category.id,
          });

          if (!isEmpty(value.result_lotto.result)) {
            speed.push(value);
          }
        }
        if (value.type === "speed_vip") {
          value.result_lotto = find(dataLotto.data.records, {
            category_id: value.category.id,
          });

          if (!isEmpty(value.result_lotto.result)) {
            speed_vip.push(value);
          }
        }
      });

      if (speed && speed_vip) {
        return { speed, speed_vip };
      } else {
        return { speed: [] as Ltobet[], speed_vip: [] as Ltobet[] };
      }
    } catch (err) {
      logger.error(err);
      return { speed: [] as Ltobet[], speed_vip: [] as Ltobet[] };
    }
  }
}

export default ScrapingWeb;
