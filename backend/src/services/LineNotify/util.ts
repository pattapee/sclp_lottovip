import { Request, Response } from "express";
import Joi from "joi";
import { getConnection } from "typeorm";

import {
  HTTPSTATUS_BADREQUEST,
  HTTPSTATUS_OK,
} from "../../constants/HttpStatus";
import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
import handleResponseError from "../Util/handleResponseError";
// import APILineNotifyChudjenbet from "./Chudjenbet";
// import APILineNotifyHuay from "./Huay";
import APILineNotifyLottoVIP from "./LottoVIP";
import APILineNotifyltobet from "./ltobet";
import APILineNotifyRuay from "./Ruay";

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class UtilLotto {
  public static triggerLineNotify = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      category: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      const { category } = value;

      await initializelottoround();
      const lottoround = await lottoroundrepository.getOne();

      switch (category) {
        case "lottovip":
          await lottoroundrepository.update(lottoround.id, { lottovip: 0 });
          APILineNotifyLottoVIP.fnInital();
          break;
        // case "huay":
        //   await lottoroundrepository.update(lottoround.id, { huay: 0 });
        //   APILineNotifyHuay.fnInital();
        //   break;
        // case "huayspeed":
        //   await lottoroundrepository.update(lottoround.id, { huayspeed: 0 });
        //   APILineNotifyHuay.fnInitalSpeed();
        //   break;
        case "ltobet":
          await lottoroundrepository.update(lottoround.id, { ltobet: 0 });
          APILineNotifyltobet.fnInital();
          break;
        case "ruay":
          await lottoroundrepository.update(lottoround.id, { ruay: 0 });
          APILineNotifyRuay.fnInital();
          break;
        // case "chudjenbetspeed":
        //   await lottoroundrepository.update(lottoround.id, { chudjenbet: 0 });
        //   APILineNotifyChudjenbet.fnInitalspeed();
        //   break;
        // case "dnabetspeed":
        //   await lottoroundrepository.update(lottoround.id, { dnabetspeed: 0 });
        //   APILineNotifyChudjenbet.fnInitalspeed();
        //   break;
      }

      return res.status(HTTPSTATUS_OK).send(true);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }
}

export default UtilLotto;
