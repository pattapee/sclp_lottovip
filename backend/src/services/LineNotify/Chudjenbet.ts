// import moment from "moment";
// import { getConnection } from "typeorm";

// import LottoResult from "../../entities/LottoResult";
// import { LottoResultRepository } from "../../repositories/LottoResultRepository";
// import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
// import logger from "../Logging";
// import { PredictivePMookController } from "../predictpmook";
// import handleMsgTxtLotto from "../Util/handleMsgTxtLotto";
// import { ScrapingWeb } from "./ScrapingWeb";
// import { ServicesLineNotify } from "./ServicesLineNotify";
// import { TypeLottoReport } from "./type";

// let lottoresultrepository: LottoResultRepository;
// const initializelottoresult = () => {
//   lottoresultrepository = getConnection().getCustomRepository(
//     LottoResultRepository
//   );
// };

// let lottoroundrepository: LottoRoundRepository;
// const initializelottoround = () => {
//   lottoroundrepository =
//     getConnection().getCustomRepository(LottoRoundRepository);
// };

// export class APILineNotifyChudjenbet {
//   public static InitalOldlengthspeed = async () => {
//     logger.info("Start InitalOldlengthspeed Chudjenbet");
//     await initializelottoround();
//     const lottoround = await lottoroundrepository.getOne();
//     const currentDate = moment(new Date());

//     if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
//       await lottoroundrepository.update(lottoround.id, {
//         chudjenbetspeed: 0,
//       });
//     } else {
//       let objData;
//       let objDataVIP;

//       do {
//         objData = (await ScrapingWeb.getDataChudjenbetSpeed()).yeekeespeed;

//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (objData.length && objData.length === lottoround.chudjenbetspeed);

//       do {
//         objDataVIP = (await ScrapingWeb.getDataChudjenbetSpeed())
//           .yeekeespeedvip;

//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (
//         objDataVIP.length &&
//         objDataVIP.length === lottoround.chudjenbetspeed
//       );

//       if (objDataVIP.length !== 0 && objDataVIP.length !== 0) {
//         if (objDataVIP.length) {
//           await lottoroundrepository.update(lottoround.id, {
//             chudjenbetspeed: objDataVIP.length,
//           });
//         }
//       }
//     }
//     logger.info("End InitalOldlengthspeed Chudjenbet");
//   }

//   public static fnInitalspeed = async () => {
//     logger.info("Start fnInitalspeed Chudjenbet");
//     await initializelottoround();
//     const lottoround = await lottoroundrepository.getOne();
//     const currentDate = moment(new Date());

//     if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
//       await lottoroundrepository.update(lottoround.id, {
//         chudjenbetspeed: 0,
//       });
//     } else {
//       let objData: TypeLottoReport[] = [];
//       let objDataVIP: TypeLottoReport[] = [];

//       do {
//         objData = (await ScrapingWeb.getDataChudjenbetSpeed()).yeekeespeed;

//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (objData.length === lottoround.chudjenbetspeed);

//       do {
//         objDataVIP = (await ScrapingWeb.getDataChudjenbetSpeed())
//           .yeekeespeedvip;

//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (objDataVIP.length === lottoround.chudjenbetspeed);

//       const { message, resultObj } = handleMsgTxtLotto(
//         `\n   💛..Chudjenbet..💛\n`,
//         "Chudjen speed",
//         objData
//       );

//       const dataMsgTxtLotto = handleMsgTxtLotto(
//         `\n💛💛Chudjenbet-VIP💛💛\n`,
//         "Chudjen speed_vip",
//         objDataVIP
//       );
//       const resultObjVIP = dataMsgTxtLotto.resultObj;
//       const messageVIP = dataMsgTxtLotto.message;

//       if (resultObj.length !== 0 && resultObjVIP.length !== 0) {
//         const lotto2cha = resultObj[resultObj.length - 1].data2;
//         const lotto3cha = resultObj[resultObj.length - 1].data3;
//         const lotto2chavip = resultObjVIP[resultObjVIP.length - 1].data2;
//         const lotto3chavip = resultObjVIP[resultObjVIP.length - 1].data3;

//         const result = {
//           name: "Chudjenbet speed",
//           data: `${lotto3cha} - ${lotto2cha}`,
//           round: objData.length,
//         };
//         const resultvip = {
//           name: "Chudjenbet speed_vip",
//           data: `${lotto3chavip} - ${lotto2chavip}`,
//           round: objDataVIP.length,
//         };

//         await ServicesLineNotify.SendLineNotifyChudjenbet(message, messageVIP);

//         await lottoroundrepository.update(lottoround.id, {
//           chudjenbetspeed: resultvip.round,
//         });

//         await initializelottoresult();
//         const lottoresult = new LottoResult();
//         lottoresult.lotto2cha = lotto2cha;
//         lottoresult.lotto3cha = lotto3cha;
//         lottoresult.name = "chudjenbetspeed";
//         lottoresult.round = objData.length;
//         lottoresultrepository.Save(lottoresult);
//         logger.info(result);

//         const lottoresultVIP = new LottoResult();
//         lottoresultVIP.lotto2cha = lotto2cha;
//         lottoresultVIP.lotto3cha = lotto3cha;
//         lottoresultVIP.name = "chudjenbetspeedvip";
//         lottoresultVIP.round = objDataVIP.length;
//         lottoresultrepository.Save(lottoresultVIP);
//         logger.info(resultvip);

//         PredictivePMookController.predictLottoSpeed("Chudjenbet").catch((err) =>
//           logger.error(err)
//         );
//       }
//     }
//     logger.info("End fnInitalspeed Chudjenbet");
//   }
// }

// export default APILineNotifyChudjenbet;
