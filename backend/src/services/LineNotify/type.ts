export interface Ltobet {
  id: number;
  lotto_category_id: number;
  admin_id: number;
  type: string;
  open_before: number;
  open_time: string;
  close_weekday: string;
  close_extra: string;
  close_holiday: string;
  close_after: number;
  close_time: string;
  result_after: number;
  result_time: string;
  result_verify: number;
  user_limit: string;
  rate_limit: LtobetRateLimit;
  summary_shoot: number;
  is_enable: number;
  created_at: string;
  updated_at: string;
  category: LtobetCategory;
  result_lotto?: LtobetResult;
  number: number;
  time: string;
  data3: number;
  data2: number;
}

interface LtobetResult {
  category_id: number;
  date: string;
  close_at: string;
  result: LtobetRateLimit;
  is_cancel: number;
}

interface LtobetRateLimit {
  three_top?: number;
  three_mix?: number;
  two_top?: number;
  two_under?: number;
  one_top?: number;
  one_under?: number;
}

interface LtobetCategory {
  id: number;
  name: string;
  flag: string;
  type: string;
  default_open_before: number;
  default_open_time: string;
  default_close_weekday: string;
  default_close_after: number;
  default_close_time: string;
  default_result_after: number;
  default_result_time: string;
  created_at: string;
  updated_at: string;
}

export interface TypeHuay {
  id: string;
  lotto_type: string;
  lotto_subtype: string;
  round_id: string;
  round_number: number;
  round_name: string;
  round_date: string;
  start_at: string;
  end_at: string;
  result_at: string;
  result: {
    first_prize: any;
    top_three: string;
    first_three: string;
    last_three: string;
    bottom_two: string;
    shooting_number_result: any;
    shooting_number_sum: string;
    shooting_number_raw: string;
    shooting_number_result_y: any;
  };
  status_paid: number;
  status_round: number;
  status_display: number;
}

export interface TypeChudjenbet {
  admin_id: number;
  id: number;
  type: string;
  category: LtobetCategory;
  is_enable: number;
  lotto_category_id: number;
}

export interface TypeLottoReport {
  data3: number;
  data2: number;
  number: number;
  time: string;
}
