// import moment from "moment";
// import { getConnection } from "typeorm";

// import LottoResult from "../../entities/LottoResult";
// import { LottoResultRepository } from "../../repositories/LottoResultRepository";
// import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
// import logger from "../Logging";
// import { PredictivePMookController } from "../predictpmook";
// import handleMsgTxtLotto from "../Util/handleMsgTxtLotto";
// import { ScrapingWeb } from "./ScrapingWeb";
// import { ServicesLineNotify } from "./ServicesLineNotify";
// import { TypeLottoReport } from "./type";

// let lottoresultrepository: LottoResultRepository;
// const initializelottoresult = () => {
//   lottoresultrepository = getConnection().getCustomRepository(
//     LottoResultRepository
//   );
// };

// let lottoroundrepository: LottoRoundRepository;
// const initializelottoround = () => {
//   lottoroundrepository =
//     getConnection().getCustomRepository(LottoRoundRepository);
// };

// export class APILineNotifyDNABet {
//   public static InitalOldlengthspeed = async () => {
//     logger.info("Start InitalOldlengthspeed DNAbet");
//     await initializelottoround();
//     const lottoround = await lottoroundrepository.getOne();
//     const currentDate = moment(new Date());
//     if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
//       await lottoroundrepository.update(lottoround.id, {
//         dnabetspeed: 0,
//       });
//     } else {
//       let objData;
//       let objDataVIP;
//       do {
//         objData = await ScrapingWeb.getDataDNABetSpeed("dnabetspeed");
//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (objData.length && objData.length === lottoround.dnabetspeed);
//       do {
//         objDataVIP = await ScrapingWeb.getDataDNABetSpeed("dnabetspeedvip");
//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (
//         objDataVIP.length &&
//         objDataVIP.length === lottoround.dnabetspeed
//       );
//       if (objData.length !== 0 && objDataVIP.length !== 0) {
//         if (objDataVIP.length) {
//           await lottoroundrepository.update(lottoround.id, {
//             dnabetspeed: objDataVIP.length,
//           });
//         }
//       }
//     }
//     logger.info("End InitalOldlengthspeed DNAbet");
//   }

//   public static fnInitalspeed = async () => {
//     logger.info("Start fnInitalspeed DNABet");
//     await initializelottoround();
//     const lottoround = await lottoroundrepository.getOne();
//     const currentDate = moment(new Date());
//     if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
//       await lottoroundrepository.update(lottoround.id, {
//         dnabetspeed: 0,
//       });
//     } else {
//       let objData: TypeLottoReport[] = [];
//       let objDataVIP: TypeLottoReport[] = [];
//       do {
//         objData = await ScrapingWeb.getDataDNABetSpeed("dnabetspeed");
//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (objData.length && objData.length === lottoround.dnabetspeed);
//       do {
//         objDataVIP = await ScrapingWeb.getDataDNABetSpeed("dnabetspeedvip");
//         await new Promise((resolve) => setTimeout(resolve, 10000));
//       } while (
//         objDataVIP.length &&
//         objDataVIP.length === lottoround.dnabetspeed
//       );

//       const { message, resultObj } = handleMsgTxtLotto(
//         `\n   🔹..DNABet..🔹\n`,
//         "DNABet speed",
//         objData
//       );
//       const dataMsgTxtLotto = handleMsgTxtLotto(
//         `\n🔷🔷DNABet-VIP🔷🔷\n`,
//         "DNABet speed_vip",
//         objDataVIP
//       );
//       const resultObjVIP = dataMsgTxtLotto.resultObj;
//       const messageVIP = dataMsgTxtLotto.message;
//       if (resultObj.length !== 0 && resultObjVIP.length !== 0) {
//         const lotto2cha = resultObj[resultObj.length - 1].data2;
//         const lotto3cha = resultObj[resultObj.length - 1].data3;
//         const lotto2chavip = resultObjVIP[resultObjVIP.length - 1].data2;
//         const lotto3chavip = resultObjVIP[resultObjVIP.length - 1].data3;
//         const result = {
//           name: "DNABet speed",
//           data: `${lotto3cha} - ${lotto2cha}`,
//           round: objData.length,
//         };
//         const resultvip = {
//           name: "DNABet speed_vip",
//           data: `${lotto3chavip} - ${lotto2chavip}`,
//           round: objDataVIP.length,
//         };
//         await ServicesLineNotify.SendLineNotifyDNABetSpeed(message, messageVIP);
//         await lottoroundrepository.update(lottoround.id, {
//           dnabetspeed: resultvip.round,
//         });

//         PredictivePMookController.predictLottoSpeed("DNABet").catch((err) =>
//           logger.error(err)
//         );
//         await initializelottoresult();
//         const lottoresult = new LottoResult();
//         lottoresult.lotto2cha = lotto2cha;
//         lottoresult.lotto3cha = lotto3cha;
//         lottoresult.name = "dnabetspeed";
//         lottoresult.round = objData.length;
//         lottoresultrepository.Save(lottoresult);
//         logger.info(result);
//         const lottoresultVIP = new LottoResult();
//         lottoresultVIP.lotto2cha = lotto2cha;
//         lottoresultVIP.lotto3cha = lotto3cha;
//         lottoresultVIP.name = "dnabetspeedvip";
//         lottoresultVIP.round = objDataVIP.length;
//         lottoresultrepository.Save(lottoresultVIP);
//         logger.info(resultvip);
//       }
//     }
//     logger.info("End fnInitalspeed DNABetSpeed");
//   }
// }

// export default APILineNotifyDNABet;
