import filter from "lodash/filter";
import moment from "moment";
import { getConnection } from "typeorm";
import LottoResult from "../../entities/LottoResult";
import { LottoResultRepository } from "../../repositories/LottoResultRepository";
import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
import logger from "../Logging";
import { ScrapingWeb } from "./ScrapingWeb";
import { ServicesLineNotify } from "./ServicesLineNotify";

let lottoresultrepository: LottoResultRepository;
const initializelottoresult = () => {
  lottoresultrepository = getConnection().getCustomRepository(
    LottoResultRepository
  );
};

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class APILineNotifyltobet {
  public static InitalOldlength = async () => {
    logger.info("Start InitalOldlength Ltobet");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { ltobet: 0 });
    } else {
      const waitUntil = async () => {
        let objDataLtobet;
        objDataLtobet = (await ScrapingWeb.getDataLtobet()).yeekee;
        await new Promise((resolve) => setTimeout(resolve, 5000));
        return objDataLtobet;
      };

      const waitUntilVIP = async () => {
        let objDataLtobetVIP;
        objDataLtobetVIP = (await ScrapingWeb.getDataLtobet()).yeekee_vip;
        await new Promise((resolve) => setTimeout(resolve, 5000));
        return objDataLtobetVIP;
      };

      const objDataLtobetresult = await waitUntil();
      const objDataLtobetVIPresult = await waitUntilVIP();

      if (objDataLtobetresult && objDataLtobetVIPresult) {
        await lottoroundrepository.update(lottoround.id, {
          ltobet: objDataLtobetVIPresult.length,
        });
      }
    }
    logger.info("End InitalOldlength Ltobet");
  }

  public static fnInital = async () => {
    logger.info("Start fnInital ltobet");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { ltobet: 0 });
    } else {
      const waitUntil = async () => {
        let objDataLtobet;

        do {
          objDataLtobet = (await ScrapingWeb.getDataLtobet()).yeekee;

          await new Promise((resolve) => setTimeout(resolve, 10000));
        } while (
          objDataLtobet.length === lottoround.ltobet &&
          objDataLtobet.length === 0
        );

        return objDataLtobet;
      };

      const waitUntilVIP = async () => {
        let objDataLtobetVIP;

        do {
          objDataLtobetVIP = (await ScrapingWeb.getDataLtobet()).yeekee_vip;

          await new Promise((resolve) => setTimeout(resolve, 10000));
        } while (
          objDataLtobetVIP.length === lottoround.ltobet &&
          objDataLtobetVIP.length === 0
        );

        return objDataLtobetVIP;
      };

      const objDataLtobetresult = await waitUntil();
      const objDataLtobetVIPresult = await waitUntilVIP();

      if (objDataLtobetresult && objDataLtobetVIPresult) {
        let headLtobet = `\n      🔷..LtoBet..🔷   \n`;
        headLtobet += `➖➖➖➖➖➖➖\n`;
        const resultObj2 = await filter(objDataLtobetresult, (value, index) => {
          return index + 1 > objDataLtobetresult.length - 10;
        });

        let DatLtobet = "";
        await resultObj2.map((value) => {
          DatLtobet += `${value.category.name.replace(
            `หวยยี่กี รอบที่ `,
            ""
          )}: ${value.category.default_close_time} ➡️ ${
            value.result_lotto.result.three_top
          } - ${value.result_lotto.result.two_under}\n`;
        });
        DatLtobet += `➖➖➖➖➖➖➖\n`;

        let headLtobetVIP = `    💜..Ltobet Vip..💜\n`;
        headLtobetVIP += `➖➖➖➖➖➖➖\n`;
        const resultObj3 = await filter(
          objDataLtobetVIPresult,
          (value, index) => {
            return index + 1 > objDataLtobetVIPresult.length - 10;
          }
        );

        let DatLtobetVIP = "";
        await resultObj3.map((value) => {
          DatLtobetVIP += `${value.category.name.replace(
            `หวยยี่กี วีไอพี รอบที่ `,
            ""
          )}: ${value.category.default_close_time} ➡️ ${
            value.result_lotto.result.three_top
          } - ${value.result_lotto.result.two_under}\n`;
        });
        DatLtobetVIP += `➖➖➖➖➖➖➖\n`;
        let AnsLtobet = `ผล Ltobet${resultObj2[
          resultObj2.length - 1
        ].category.name.replace(`หวยยี่กี`, "")}  \n`;
        AnsLtobet += `         ${
          resultObj2[resultObj2.length - 1].result_lotto.result.three_top
        } - ${
          resultObj2[resultObj2.length - 1].result_lotto.result.two_under
        } \n`;
        AnsLtobet += `➖➖➖➖➖➖➖\n`;

        let AnsLtobetVIP = `ผล Ltobet VIP${resultObj3[
          resultObj3.length - 1
        ].category.name.replace(`หวยยี่กี วีไอพี`, "")}  \n`;
        AnsLtobetVIP += `         ${
          resultObj3[resultObj3.length - 1].result_lotto.result.three_top
        } - ${
          resultObj3[resultObj3.length - 1].result_lotto.result.two_under
        } \n`;
        AnsLtobetVIP += `➖➖➖➖➖➖➖\n`;

        const SumMessage =
          headLtobet +
          DatLtobet +
          headLtobetVIP +
          DatLtobetVIP +
          AnsLtobet +
          AnsLtobetVIP;
        const LtobetMessage = headLtobet + DatLtobet + AnsLtobet;
        const LtobetVIPMessage =
          "\n" + headLtobetVIP + DatLtobetVIP + AnsLtobetVIP;

        if (resultObj2.length !== 0 && resultObj3.length !== 0) {
          const result2lotto2cha =
            resultObj2[resultObj2.length - 1].result_lotto.result.two_under;
          const result2lotto3cha =
            resultObj2[resultObj2.length - 1].result_lotto.result.three_top;

          const result2 = {
            name: "Ltobet Yeekee",
            data: `${result2lotto3cha} - ${result2lotto2cha}`,
            round: objDataLtobetresult.length,
          };

          const result3lotto2cha =
            resultObj3[resultObj3.length - 1].result_lotto.result.two_under;
          const result3lotto3cha =
            resultObj3[resultObj3.length - 1].result_lotto.result.three_top;

          const result3 = {
            name: "Ltobet Yeekee VIP",
            data: `${result3lotto3cha} - ${result3lotto2cha}`,
            round: objDataLtobetVIPresult.length,
          };

          await ServicesLineNotify.SendLineNotifyLtobetYeekee(
            SumMessage,
            LtobetMessage,
            LtobetVIPMessage
          );

          if (resultObj2.length) {
            await lottoroundrepository.update(lottoround.id, {
              ltobet: objDataLtobetresult.length,
            });

            await initializelottoresult();
            const result = new LottoResult();
            result.lotto2cha = result2lotto2cha;
            result.lotto3cha = result2lotto3cha;
            result.name = "ltobet_yeekee";
            result.round = result2.round;
            lottoresultrepository.Save(result);
            logger.info(result2);

            const VIPResult = new LottoResult();
            VIPResult.lotto2cha = result3lotto2cha;
            VIPResult.lotto3cha = result3lotto3cha;
            VIPResult.name = "ltobet_yeekee_vip";
            VIPResult.round = objDataLtobetresult.length;
            lottoresultrepository.Save(VIPResult);
            logger.info(result3);
          }
        }
      }
    }
    logger.info("End fnInital ltobet: ");
  }
}

export default APILineNotifyltobet;
