// import includes from "lodash/includes";
import compact from "lodash/compact";
import moment from "moment";
import { getConnection } from "typeorm";

import LottoResult from "../../entities/LottoResult";
import PredictiveLotto from "../../entities/PredictiveLotto";
import { LottoResultRepository } from "../../repositories/LottoResultRepository";
import { LottoRoundRepository } from "../../repositories/LottoRoundRepository";
import { PredictiveLottoRepository } from "../../repositories/PredictiveLottoRepository";
import logger from "../Logging";
import handleMsgTxtLotto from "../Util/handleMsgTxtLotto";
// import numberIncrease from "../Util/numberIncreaseTxt";
// import tranformResultLotto from "../Util/tranformResultLotto";
// import APILineNotifyHuay from "./Huay";
import { ScrapingWeb } from "./ScrapingWeb";
import { ServicesLineNotify } from "./ServicesLineNotify";
import { TypeLottoReport } from "./type";

let lottoresultrepository: LottoResultRepository;
const initializelottoresult = () => {
  lottoresultrepository = getConnection().getCustomRepository(
    LottoResultRepository
  );
};

let predictivelottorepository: PredictiveLottoRepository;
const initialzepredictivelotto = () => {
  predictivelottorepository = getConnection().getCustomRepository(
    PredictiveLottoRepository
  );
};

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class APILineNotifyLottoVIP {
  public static InitalOldlength = async () => {
    logger.info("Start InitalOldlength LottoVIP");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { lottovip: 0 });
    } else {
      const getDataLottoVIP = async () => {
        let Lotto: TypeLottoReport[] = [];
        await new Promise((resolve) => setTimeout(resolve, 5000));
        Lotto = compact(await ScrapingWeb.getDataLottoVIP());
        return Lotto;
      };

      const objDataLottoVIPresult = await getDataLottoVIP();

      if (objDataLottoVIPresult) {
        await lottoroundrepository.update(lottoround.id, {
          lottovip: objDataLottoVIPresult.length,
        });
      }
    }
    logger.info("End InitalOldlength LottoVIP");
  }

  public static fnInital = async () => {
    logger.info("Start fnInital LottoVIP");
    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
      await lottoroundrepository.update(lottoround.id, { lottovip: 0 });
    } else {
      const getDataLottoVIP = async () => {
        let Lotto: TypeLottoReport[] = [];
        do {
          await new Promise((resolve) => setTimeout(resolve, 5000));
          Lotto = compact(await ScrapingWeb.getDataLottoVIP());
        } while (Lotto.length === lottoround.lottovip || Lotto.length === 0);

        return Lotto;
      };

      const objDataLottoVIPresult = await getDataLottoVIP();

      if (objDataLottoVIPresult) {
        const { message, resultObj } = await handleMsgTxtLotto(
          `\n     💘Lotto Vip.💘\n`,
          "LottoVip",
          objDataLottoVIPresult
        );
        const lotto2cha = resultObj[resultObj.length - 1].data2;
        const lotto3cha = resultObj[resultObj.length - 1].data3;

        const result = {
          name: "Lotto VIP",
          data: `${lotto3cha} - ${lotto2cha}`,
          round: objDataLottoVIPresult.length,
        };

        await ServicesLineNotify.SendLineNotify(message);

        await lottoroundrepository.update(lottoround.id, {
          lottovip: result.round,
        });

        await initializelottoresult();
        const lottoresult = new LottoResult();
        lottoresult.lotto2cha = lotto2cha;
        lottoresult.lotto3cha = lotto3cha;
        lottoresult.name = "lottovip";
        lottoresult.round = Number(result.round);
        lottoresultrepository.Save(lottoresult);

        if (result.round <= 88) {
          await APILineNotifyLottoVIP.predictiveLottoVIP();
        }

        logger.info(result);
      }
    }
    logger.info("End fnInital LottoVIP");
  }

  public static lastInital = async () => {
    const currentDate = moment(new Date());

    if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
    } else {
      const obj = await ScrapingWeb.getDataLottoVIP();

      if (obj.length !== 0) {
        await ServicesLineNotify.sendMsgCountDown();
      }
    }
  }

  public static predictiveLottoVIP = async () => {
    const currentDate = moment(new Date());

    await new Promise((resolve) => setTimeout(resolve, 40000));

    await initializelottoround();
    const lottoround = await lottoroundrepository.getOne();

    try {
      if (currentDate.hour() >= 4 && currentDate.hour() <= 5) {
        return "";
      } else {
        try {
          // const objDataLottoVIP = await ScrapingWeb.getDataLottoVIP();

          // let formulaFirst1 = "\nเสียวตัวเดียว | สูตร1 =>";
          // let formulaSecond1 = "\nเสียวตัวเดียว | สูตร2 =>";
          // // const formulaFirst6 = "\nปัก6 | สูตร1 =>";
          // // const formulaSecond6 = "\nปัก6 | สูตร2 =>";

          // for (let i = 10; i > 0; i--) {
          //   const currentdatPredictive =
          //     await ServicesLineNotify.predictiveData(
          //       String(lottoround.lottovip - i),
          //       "lottovip"
          //     );
          // const currentLottoVIP = objDataLottoVIP[objDataLottoVIP.length - i];
          // const { result3, result2 } = tranformResultLotto(
          //   currentLottoVIP.data3,
          //   currentLottoVIP.data2
          // );
          //   const chkPredict = includes(
          //     `${result3}${result2}`,
          //     currentdatPredictive[0].number.toString()
          //   )
          //     ? `✅`
          //     : ``;
          //   formulaFirst1 += `\n${
          //     currentLottoVIP.number
          //   } = ${currentdatPredictive[0].number.toString()} |${result3}-${result2}| ${chkPredict}`;

          //   const chkPredict2 = includes(
          //     `${result3}${result2}`,
          //     currentdatPredictive[1].number.toString()
          //   )
          //     ? `✅`
          //     : ``;

          //   formulaSecond1 += `\n${
          //     currentLottoVIP.number
          //   } = ${currentdatPredictive[1].number.toString()} |${result3}-${result2}| ${chkPredict2}`;

          // const chkPredict3 = includes(
          //   `${numberIncrease(Number(currentdatPredictive[0].number), 6)}`,
          //   currentdatPredictive[0].number.toString()
          // )
          //   ? `✅`
          //   : ``;

          // const chkPredict4 = includes(
          //   `${numberIncrease(Number(currentdatPredictive[1].number), 6)}`,
          //   currentdatPredictive[1].number.toString()
          // )
          //   ? `✅`
          //   : ``;

          // formulaFirst6 += `\n${currentLottoVIP.number}: ${numberIncrease(
          //   Number(currentdatPredictive[0].number),
          //   6
          // )} |${result3}-${result2}| ${chkPredict3}`;

          // formulaSecond6 += `\n${currentLottoVIP.number}: ${numberIncrease(
          //   Number(currentdatPredictive[1].number),
          //   6
          // )} |${result3}-${result2}| ${chkPredict4}`;
          // }

          // const dataPredictive = await ServicesLineNotify.predictiveData(
          //   String(lottoround.lottovip + 1),
          //   "lottovip"
          // );
          // let message = `🚩 LottoVIP 🚩`;
          // message += `\n➖➖➖➖➖➖➖`;
          // message += formulaFirst1;
          // message += `\n${Number(lottoround.lottovip) + 1} = ${
          //   dataPredictive[0].number
          // } =>`;
          // message += `\n➖➖➖➖➖➖➖`;
          // message += formulaSecond1;
          // message += `\n${Number(lottoround.lottovip) + 1} = ${
          //   dataPredictive[1].number
          // } =>`;
          // message += `\n➖➖➖➖➖➖➖`;

          // message += `\nรอบที่: ${Number(lottoround.lottovip) + 1} =>`;
          // message += `\nสูตร1 => ${dataPredictive[0].number} - ${numberIncrease(
          //   Number(dataPredictive[0].number),
          //   5
          // )}`;
          // message += `\nสูตร2 => ${dataPredictive[1].number} - ${numberIncrease(
          //   Number(dataPredictive[1].number),
          //   5
          // )}`;
          // message += `\n➖➖➖➖➖➖➖`;

          const dataPredictive = await ServicesLineNotify.predictiveData(
            String(lottoround.lottovip + 1),
            "lottovip"
          );
          let message = `\n  🚩🚩 LottoVIP 🚩`;
          message += `\n➖➖➖➖➖➖➖`;
          message += `\nรอบที่ ${lottoround.lottovip + 1} => ${
            dataPredictive[0].number
          } - ${dataPredictive[1].number}`;
          message += `\n➖➖➖➖➖➖➖`;

          await ServicesLineNotify.sendMsgPredictLottoVIP(message);

          await initialzepredictivelotto();

          const dataPredictiveLotto = new PredictiveLotto();
          dataPredictiveLotto.datapredict = JSON.stringify(dataPredictive);
          dataPredictiveLotto.name = "lottovip";
          dataPredictiveLotto.round = +(lottoround.lottovip + 1);
          predictivelottorepository.Save(dataPredictiveLotto);
        } catch (err) {
          logger.error(err);
        }
      }
    } catch (err) {
      logger.error(err);
    }
  }
}

export default APILineNotifyLottoVIP;
