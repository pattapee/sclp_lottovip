import { Request, Response } from "express";
import Joi from "joi";
import { getConnection } from "typeorm";

import {
  HTTPSTATUS_ACCEPT,
  HTTPSTATUS_CREATE,
  HTTPSTATUS_OK,
} from "../constants/HttpStatus";
import PredictPMook from "../entities/PredictPMook";
import LottoRoundRepository from "../repositories/LottoRoundRepository";
import { PredictPMookRepository } from "../repositories/PredictPMook";
import ScrapingWeb from "./LineNotify/ScrapingWeb";
import ServicesLineNotify from "./LineNotify/ServicesLineNotify";
import { TypeLottoReport } from "./LineNotify/type";
import logger from "./Logging";
import {
  Calfunction1,
  Calfunction10,
  Calfunction2,
  Calfunction3,
  Calfunction4,
  Calfunction5,
  Calfunction6,
  Calfunction7,
  Calfunction8,
  Calfunction9,
} from "./PredictPMooK/CalMultifunction";
import handleResponseError from "./Util/handleResponseError";

let predictpmookrepository: PredictPMookRepository;
const initializepredictpmook = () => {
  predictpmookrepository = getConnection().getCustomRepository(
    PredictPMookRepository
  );
};

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

export class PredictivePMookController {
  public static save = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      linetoken: Joi.string().allow(""),
      contactHeadGroup: Joi.string().allow(""),
      endDate: Joi.date().required(),
      remark: Joi.string().allow(""),
      function1: Joi.boolean().required(),
      function1TypePredictived: Joi.number().required(),
      function2: Joi.boolean().required(),
      function2TypePredictived: Joi.number().required(),
      function3: Joi.boolean().required(),
      function3TypePredictived: Joi.number().required(),
      function4: Joi.boolean().required(),
      function4TypePredictived: Joi.number().required(),
      function5: Joi.boolean().required(),
      function5TypePredictived: Joi.number().required(),
      function6: Joi.boolean().required(),
      function6TypePredictived: Joi.number().required(),
      function7: Joi.boolean().required(),
      function7TypePredictived: Joi.number().required(),
      function8: Joi.boolean().required(),
      function8TypePredictived: Joi.number().required(),
      function9: Joi.boolean().required(),
      function9TypePredictived: Joi.number().required(),
      function10: Joi.boolean().required(),
      function10TypePredictived: Joi.number().required(),
      webOwner: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const data = new PredictPMook();
      data.name = value.name;
      data.linetoken = value.linetoken;
      data.contactHeadGroup = value.contactHeadGroup;
      data.endDate = value.endDate;
      data.remark = value.remark;
      data.function1 = value.function1;
      data.function1TypePredictived = value.function1TypePredictived;
      data.function2 = value.function2;
      data.function2TypePredictived = value.function2TypePredictived;
      data.function3 = value.function3;
      data.function3TypePredictived = value.function3TypePredictived;
      data.function4 = value.function4;
      data.function4TypePredictived = value.function4TypePredictived;
      data.function5 = value.function5;
      data.function5TypePredictived = value.function5TypePredictived;
      data.function6 = value.function6;
      data.function6TypePredictived = value.function6TypePredictived;
      data.function7 = value.function7;
      data.function7TypePredictived = value.function7TypePredictived;
      data.function8 = value.function8;
      data.function8TypePredictived = value.function8TypePredictived;
      data.function9 = value.function9;
      data.function9TypePredictived = value.function9TypePredictived;
      data.function10 = value.function10;
      data.function10TypePredictived = value.function10TypePredictived;
      data.isActived = true;
      data.webOwner = value.webOwner;

      await initializepredictpmook();
      const result = await predictpmookrepository.Save(data);
      return res.status(HTTPSTATUS_CREATE).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static autoCreateGroupPak1 = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);
      await initializepredictpmook();
      const lotto = await predictpmookrepository.getOneByID(value.id);
      if (lotto.function1) {
        lotto.function1TypePredictived = 1;
      }
      if (lotto.function2) {
        lotto.function2TypePredictived = 1;
      }
      if (lotto.function3) {
        lotto.function3TypePredictived = 1;
      }
      if (lotto.function4) {
        lotto.function4TypePredictived = 1;
      }
      if (lotto.function5) {
        lotto.function5TypePredictived = 1;
      }
      if (lotto.function6) {
        lotto.function6TypePredictived = 1;
      }
      if (lotto.function7) {
        lotto.function7TypePredictived = 1;
      }
      if (lotto.function8) {
        lotto.function8TypePredictived = 1;
      }
      if (lotto.function9) {
        lotto.function9TypePredictived = 1;
      }
      if (lotto.function10) {
        lotto.function10TypePredictived = 1;
      }
      lotto.createdAt = new Date();
      lotto.updatedAt = new Date();
      delete lotto.id;

      const result = await predictpmookrepository.Save(lotto);

      return res.status(HTTPSTATUS_CREATE).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static update = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
      name: Joi.string().required(),
      linetoken: Joi.string().allow(""),
      contactHeadGroup: Joi.string().allow(""),
      endDate: Joi.date().required(),
      remark: Joi.string().allow(""),
      function1: Joi.boolean().required(),
      function1TypePredictived: Joi.number().required(),
      function2: Joi.boolean().required(),
      function2TypePredictived: Joi.number().required(),
      function3: Joi.boolean().required(),
      function3TypePredictived: Joi.number().required(),
      function4: Joi.boolean().required(),
      function4TypePredictived: Joi.number().required(),
      function5: Joi.boolean().required(),
      function5TypePredictived: Joi.number().required(),
      function6: Joi.boolean().required(),
      function6TypePredictived: Joi.number().required(),
      function7: Joi.boolean().required(),
      function7TypePredictived: Joi.number().required(),
      function8: Joi.boolean().required(),
      function8TypePredictived: Joi.number().required(),
      function9: Joi.boolean().required(),
      function9TypePredictived: Joi.number().required(),
      function10: Joi.boolean().required(),
      function10TypePredictived: Joi.number().required(),
      webOwner: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      await initializepredictpmook();
      const data = await predictpmookrepository.getOneByID(value.id);
      data.name = value.name;
      data.remark = value.remark;
      data.function1 = value.function1;
      data.endDate = value.endDate;
      data.function1TypePredictived = value.function1TypePredictived;
      data.function2 = value.function2;
      data.function2TypePredictived = value.function2TypePredictived;
      data.function3 = value.function3;
      data.function3TypePredictived = value.function3TypePredictived;
      data.function4 = value.function4;
      data.function4TypePredictived = value.function4TypePredictived;
      data.function5 = value.function5;
      data.function5TypePredictived = value.function5TypePredictived;
      data.function6 = value.function6;
      data.function6TypePredictived = value.function6TypePredictived;
      data.function7 = value.function7;
      data.function7TypePredictived = value.function7TypePredictived;
      data.function8 = value.function8;
      data.function8TypePredictived = value.function8TypePredictived;
      data.function9 = value.function9;
      data.function9TypePredictived = value.function9TypePredictived;
      data.function10 = value.function10;
      data.function10TypePredictived = value.function10TypePredictived;
      data.webOwner = value.webOwner;

      const result = await predictpmookrepository.Update(value.id, data);
      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static updateisActived = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
      status: Joi.boolean().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      await initializepredictpmook();
      const lotto = await predictpmookrepository.getOneByID(value.id);
      lotto.isActived = value.status;
      const result = await predictpmookrepository.Update(value.id, lotto);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static delete = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initializepredictpmook();
      const lotto = await predictpmookrepository.getOneByID(value.id);
      const result = await predictpmookrepository.Delete(lotto);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getOneByID = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initializepredictpmook();
      const result = await predictpmookrepository.getOneByID(value.id);

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getAll = async (req: Request, res: Response) => {
    try {
      await initializepredictpmook();
      const result = await predictpmookrepository.getAll();

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  // public static predictLottoSpeed = async (webOwner: string) => {
  //   logger.info("Start predictChudjenbetPMook");

  //   await initializelottoround();

  //   let objData: TypeLottoReport[] = [];
  //   switch (webOwner) {
  //     case "Chudjenbet":
  //       objData = (await ScrapingWeb.getDataChudjenbetSpeed()).yeekeespeed;
  //       break;
  //     case "DNABet":
  //       objData = await ScrapingWeb.getDataDNABetSpeed("dnabetspeed");
  //       break;
  //   }

  //   if (objData.length > 1) {
  //     await initializepredictpmook();
  //     const result = await predictpmookrepository.getAllIsActivedBywebOwner(
  //       webOwner
  //     );

  //     result.forEach(async (value: PredictPMook) => {
  //       const {
  //         function1,
  //         function2,
  //         function3,
  //         function4,
  //         function5,
  //         function6,
  //         function7,
  //         function8,
  //         function9,
  //         function10,
  //       } = value;

  //       if (
  //         function1 ||
  //         function2 ||
  //         function3 ||
  //         function4 ||
  //         function5 ||
  //         function6 ||
  //         function7 ||
  //         function8 ||
  //         function9 ||
  //         function10
  //       ) {
  //         let MsgTxtPMook = `\n 🚩🚩${value.webOwner}5นาที\n`;
  //         MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //         const byMook = `By @MOOK\n`;
  //         const typeFunctionMaster = [
  //           "เสียวตัวเดียว",
  //           "ปักสิบบน",
  //           "ปักหน่วยบน",
  //           "ปักสิบล่าง",
  //           "ปักหน่วยล่าง",
  //         ];

  //         if (function1) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function1TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction1(
  //             objData,
  //             value.function1TypePredictived
  //           );
  //           MsgTxtPMook += `\nC1 ${byMook}`;
  //         }

  //         if (function2) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function2TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction2(
  //             objData,
  //             value.function2TypePredictived
  //           );
  //           MsgTxtPMook += `\nC2 ${byMook}`;
  //         }

  //         if (function3) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function3TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction3(
  //             objData,
  //             value.function3TypePredictived
  //           );
  //           MsgTxtPMook += `\nC3 ${byMook}`;
  //         }

  //         if (function4) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function4TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction4(
  //             objData,
  //             value.function4TypePredictived
  //           );
  //           MsgTxtPMook += `\nC4 ${byMook}`;
  //         }

  //         if (function5) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function5TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction5(
  //             objData,
  //             value.function5TypePredictived
  //           );
  //           MsgTxtPMook += `\nC5 ${byMook}`;
  //         }

  //         if (function6) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function6TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction6(
  //             objData,
  //             value.function6TypePredictived
  //           );
  //           MsgTxtPMook += `\nC6 ${byMook}`;
  //         }

  //         if (function7) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function7TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction7(
  //             objData,
  //             value.function7TypePredictived
  //           );
  //           MsgTxtPMook += `\nC7 ${byMook}`;
  //         }

  //         if (function8) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function8TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction8(
  //             objData,
  //             value.function8TypePredictived
  //           );
  //           MsgTxtPMook += `\nC8 ${byMook}`;
  //         }

  //         if (function9) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function9TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction9(
  //             objData,
  //             value.function9TypePredictived
  //           );
  //           MsgTxtPMook += `\nC9 ${byMook}`;
  //         }
  //         if (function10) {
  //           MsgTxtPMook += `${
  //             value.webOwner +
  //             " " +
  //             typeFunctionMaster[value.function10TypePredictived - 1]
  //           }\n`;
  //           MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //           MsgTxtPMook += await Calfunction10(
  //             objData,
  //             value.function10TypePredictived
  //           );
  //           MsgTxtPMook += `\nC10 ${byMook}`;
  //         }

  //         if (value.remark && value.remark !== "") {
  //           MsgTxtPMook += `${value.remark}\n`;
  //         }
  //         MsgTxtPMook += `➖➖➖➖➖➖➖\n`;
  //         await ServicesLineNotify.APISendLinenotify(
  //           MsgTxtPMook,
  //           value.linetoken
  //         );
  //       }
  //     });

  //     logger.info("End predictChudjenbetPMook");
  //   }
  // }
}

export default PredictivePMookController;
