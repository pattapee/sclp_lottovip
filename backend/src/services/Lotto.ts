import { Request, Response } from "express";
import Joi from "joi";
import { getConnection } from "typeorm";

import {
  HTTPSTATUS_ACCEPT,
  HTTPSTATUS_CREATE,
  HTTPSTATUS_OK,
} from "../constants/HttpStatus";
import { Lotto } from "../entities/Lotto";
import { LottoRepository } from "../repositories/LottoRepository";
import handleResponseError from "./Util/handleResponseError";

let repository: LottoRepository;
const initialize = () => {
  repository = getConnection().getCustomRepository(LottoRepository);
};

export class LottoController {
  public static getAll = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      category: Joi.string().required(),
      isactive: Joi.boolean().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      const { category, isactive } = value;

      await initialize();
      const result = await repository.getAllByCategoryAndIsActive(
        category,
        isactive ? true : false
      );

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getAllByActive = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      category: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      const { category } = value;

      await initialize();
      const result = await repository.getAllByCategoryAndIsActive(
        category,
        true
      );

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getAllByMoreThanendDate = async (
    req: Request,
    res: Response
  ) => {
    const schema = Joi.object().keys({
      category: Joi.string().required(),
      endDate: Joi.date().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const { endDate, category } = value;

      await initialize();
      const result = await repository.getAllByMoreThanendDate(
        endDate,
        category
      );

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static getOneByID = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initialize();
      const result = await repository.getOneByID(value.id);

      return res.status(HTTPSTATUS_OK).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static saveLotto = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      linetoken: Joi.string().required(),
      contactHeadGroup: Joi.string().allow(""),
      endDate: Joi.date().required(),
      remark: Joi.string().allow(""),
      name: Joi.string().required(),
      category: Joi.string().required(),
      // huay: Joi.boolean().required(),
      // huayvip: Joi.boolean().required(),
      // huayspeed: Joi.boolean().required(),
      // huayspeedvip: Joi.boolean().required(),
      ltobetyeekee: Joi.boolean().required(),
      ltobetyeekeevip: Joi.boolean().required(),
      ltobetspeed: Joi.boolean().required(),
      ltobetspeedvip: Joi.boolean().required(),
      // chudjenbet: Joi.boolean().required(),
      // chudjenbetvip: Joi.boolean().required(),
      dnabet: Joi.boolean().required(),
      dnabetspeed: Joi.boolean().required(),
      isPredictived: Joi.boolean().allow(""),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const data = new Lotto();
      data.linetoken = value.linetoken;
      data.contactHeadGroup = value.contactHeadGroup;
      data.endDate = value.endDate;
      data.remark = value.remark;
      const categoryWeb = value.category;
      data.category = categoryWeb;
      data.name = value.name;
      data.isPredictived = value.isPredictived;
      // data.huay = value.huay;
      // data.huayvip = value.huayvip;
      // data.huayspeed = value.huayspeed;
      // data.huayspeedvip = value.huayspeedvip;
      data.ltobetyeekee = value.ltobetyeekee;
      data.ltobetyeekeevip = value.ltobetyeekeevip;
      data.ltobetspeed = value.ltobetspeed;
      data.ltobetspeedvip = value.ltobetspeedvip;
      data.dnabet = value.dnabet;
      data.dnabetspeed = value.dnabetspeed;
      // data.chudjenbet = value.chudjenbet;
      // data.chudjenbetvip = value.chudjenbetvip;
      data.isActive = true;

      await initialize();
      const result = await repository.Save(data);

      return res.status(HTTPSTATUS_CREATE).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static updateLotto = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      linetoken: Joi.string().required(),
      contactHeadGroup: Joi.string().allow(""),
      endDate: Joi.date().required(),
      remark: Joi.string().allow(),
      id: Joi.string().required(),
      name: Joi.string().required(),
      isPredictived: Joi.boolean().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const {
        linetoken,
        contactHeadGroup,
        endDate,
        remark,
        id,
        name,
        isPredictived,
      } = value;

      const newData = new Lotto();
      newData.linetoken = linetoken;
      newData.contactHeadGroup = contactHeadGroup;
      newData.endDate = endDate;
      newData.remark = remark;
      newData.name = name;
      newData.isPredictived = isPredictived;

      await initialize();
      const result = await repository.Update(id, newData);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static updateActived = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
      isActive: Joi.boolean().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);

      const data = new Lotto();
      data.isActive = value.isActive;
      data.id = value.id;

      await initialize();
      const result = await repository.Update(req.body.id, data);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }

  public static delete = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      id: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.params);

      await initialize();
      const lotto = await repository.getOneByID(value.id);
      const result = await repository.Delete(lotto);

      return res.status(HTTPSTATUS_ACCEPT).send(result);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(dataError.status).send({ data: dataError.data });
    }
  }
}

export default LottoController;
