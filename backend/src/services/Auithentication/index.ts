import { validate } from "class-validator";
import dotenv from "dotenv";
import { Request, Response } from "express";
import Joi from "joi";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";

import {
  HTTPSTATUS_BADREQUEST,
  HTTPSTATUS_NOCONTENT,
  HTTPSTATUS_OK,
  HTTPSTATUS_UNAUTHORIZED,
} from "../../constants/HttpStatus";
import { User } from "../../entities/User";
import handleResponseError from "../Util/handleResponseError";

dotenv.config();

export default class AuthenServices {
  public static login = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      username: Joi.string().required(),
      password: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);
      const { username, password } = value;

      const userRepository = getRepository(User);
      let user: User;

      user = await userRepository.findOneOrFail({ where: { username } });

      if (user.checkIfUnencryptedPasswordIsValid(password)) {
        const token = jwt.sign(
          { userId: user.id, username: user.username, isActive: user.isActive },
          process.env.jwtSecret,
          { expiresIn: "1h" }
        );

        return res
          .status(HTTPSTATUS_OK)
          .send({ token, role: user.role, username: user.username });
      } else {
        return res.status(HTTPSTATUS_UNAUTHORIZED).send(false);
      }
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(HTTPSTATUS_UNAUTHORIZED).send({ data: dataError.data });
    }
  }

  public static checkToken = async (req: Request, res: Response) => {
    const schema = Joi.object().keys({
      usertoken: Joi.string().required(),
      userrole: Joi.string().required(),
    });

    try {
      const value = await schema.validateAsync(req.body);
      const { usertoken } = value;

      await jwt.verify(usertoken, process.env.jwtSecret);
      return res.status(HTTPSTATUS_OK).send(true);
    } catch (err) {
      const dataError = handleResponseError(err);

      return res.status(HTTPSTATUS_UNAUTHORIZED).send({ data: dataError.data });
    }
  }

  public static changePassword = async (req: Request, res: Response) => {
    const id = res.locals.jwtPayload.userId;

    const { oldPassword, newPassword } = req.body;
    if (!(oldPassword && newPassword)) {
      res.status(HTTPSTATUS_BADREQUEST).send(false);
    }

    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      res.status(HTTPSTATUS_UNAUTHORIZED).send(false);
    }

    if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
      res.status(HTTPSTATUS_UNAUTHORIZED).send(false);
      return;
    }

    user.password = newPassword;
    const errors = await validate(user);
    if (errors.length > 0) {
      res.status(HTTPSTATUS_BADREQUEST).send(errors);
      return;
    }
    user.hashPassword();
    userRepository.save(user);

    res.status(HTTPSTATUS_NOCONTENT).send(false);
  }
}
