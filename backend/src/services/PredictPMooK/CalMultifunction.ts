import find from "lodash/find";
import includes from "lodash/includes";
import last from "lodash/last";
import slice from "lodash/slice";
import { TypeLottoReport } from "../LineNotify/type";
import numberIncrease from "../Util/numberIncreaseTxt";
import tranformResultLotto from "../Util/tranformResultLotto";

const splitFromLastObjData = (objData: TypeLottoReport[], length: number) => {
  if (objData.length > length) {
    return slice(objData, objData.length - length, objData.length);
  } else {
    return objData;
  }
};

const fullCalfunction = (
  objData: TypeLottoReport[],
  fnCalculator: any,
  type: number
) => {
  let msgTxt = "";

  const objDataSplit = splitFromLastObjData(objData, 5);

  objDataSplit.forEach((result: TypeLottoReport) => {
    const lastedObjData = find(objData, { number: result.number - 1 });
    const predictNumber = fnCalculator(lastedObjData);

    const { result3, result2 } = tranformResultLotto(
      result.data3,
      result.data2
    );
    const numberPakUp = result3.split("");
    const numberPakLow = result2.split("");

    /* Type
    1 = เสียวตัวเดียว
    2 = ปักสิบบน
    3 = ปักหน่วยบน
    4 = ปักสิบล่าง
    5 = ปักหน่วยล่าง
    */

    let chkPredict = "";
    let test = "";

    switch (type) {
      case 1:
        test = predictNumber;
        chkPredict = includes(`${result3}${result2}`, predictNumber)
          ? `✅`
          : ``;
        break;
      case 2:
        test = numberPakUp[1].toString();
        chkPredict = includes(`${predictNumber}`, numberPakUp[1].toString())
          ? `✅`
          : ``;
        break;
      case 3:
        test = numberPakUp[2].toString();
        chkPredict = includes(`${predictNumber}`, numberPakUp[2].toString())
          ? `✅`
          : ``;
        break;
      case 4:
        test = numberPakLow[0].toString();
        chkPredict = includes(`${predictNumber}`, numberPakLow[0].toString())
          ? `✅`
          : ``;
        break;
      case 5:
        test = numberPakLow[1].toString();
        chkPredict = includes(`${predictNumber}`, numberPakLow[1].toString())
          ? `✅`
          : ``;
        break;
    }

    msgTxt += `${result.number}: ${predictNumber} |${result3}-${result2}| ${chkPredict}\n`;
  });

  const objDataSplitLast = last(objDataSplit);
  const predictNumberNext = fnCalculator(objDataSplitLast);
  msgTxt += "➖➖➖➖➖➖➖\n";
  msgTxt += `รอบที่ ${objDataSplitLast.number + 1} => ${predictNumberNext}\n`;

  return msgTxt;
};

export async function Calfunction1(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result2.split("")[0]) + 6).toString().split("");

    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction2(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3, result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (
      Number(result3.split("")[2]) +
      Number(result2.split("")[0]) +
      5
    )
      .toString()
      .split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction3(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result2.split("")[0]) + 4).toString().split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction4(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result3.split("")[2]) + 4).toString().split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction5(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result3.split("")[1]) + 7).toString().split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction6(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3, result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (
      Number(result3.split("")[0]) + Number(result2.split("")[1])
    )
      .toString()
      .split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction7(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (
      Number(result2.split("")[0]) + Number(result2.split("")[0])
    )
      .toString()
      .split("");
    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction8(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result2 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result2.split("")[0]) + 1).toString().split("");

    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction9(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (
      Number(result3.split("")[0]) +
      Number(result3.split("")[1]) +
      4
    )
      .toString()
      .split("");

    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}

export async function Calfunction10(
  objData: TypeLottoReport[],
  typeFunction: number
) {
  const fnCalculator = (lotto: TypeLottoReport) => {
    const { result3 } = tranformResultLotto(lotto.data3, lotto.data2);
    const predictData = (Number(result3.split("")[1]) + 8).toString().split("");

    const predictResult =
      predictData.length > 1 ? predictData[1] : predictData[0];

    return typeFunction === 1
      ? predictResult
      : numberIncrease(Number(predictResult), 6);
  };

  return fullCalfunction(objData, fnCalculator, typeFunction);
}
