import dotenv from "dotenv";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";

dotenv.config();

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  // Get the jwt token from the head
  const token = req.headers.auth as string;
  let jwtPayload;
  // Try to validate the token and get data
  try {
    jwtPayload = jwt.verify(token, process.env.jwtSecret) as any;
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    // If token is not valid, respond with 401 (unauthorized)
    res.status(401).send({ msg: "Token invalid, Please check your token." });
    return {};
  }

  // The token is valid for 3 hour
  // We want to send a new token on every request
  const { userId, username } = jwtPayload;
  const newToken = jwt.sign({ userId, username }, process.env.jwtSecret, {
    expiresIn: "6h",
  });
  res.setHeader("token", newToken);

  // Call the next middleware or controller
  next();
};
