import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class LottoRound {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public lottovip: number;

  @Column()
  public huay: number;

  @Column()
  public huayspeed: number;

  @Column()
  public ltobet: number;

  @Column()
  public ltobetspeed: number;

  @Column()
  public ruay: number;

  @Column()
  public chudjenbet: number;

  @Column()
  public chudjenbetspeed: number;

  @Column()
  public dnabet: number;

  @Column()
  public dnabetspeed: number;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}

export default LottoRound;
