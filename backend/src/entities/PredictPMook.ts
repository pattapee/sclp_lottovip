import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class PredictPMook {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public name: string;

  @Column()
  public linetoken: string;

  @Column()
  public contactHeadGroup: string;

  @Column()
  public endDate: Date;

  @Column({ nullable: true })
  public remark: string;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function1: boolean;
  @Column()
  public function1TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function2: boolean;
  @Column()
  public function2TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function3: boolean;
  @Column()
  public function3TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function4: boolean;
  @Column()
  public function4TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function5: boolean;
  @Column()
  public function5TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function6: boolean;
  @Column()
  public function6TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function7: boolean;
  @Column()
  public function7TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function8: boolean;
  @Column()
  public function8TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function9: boolean;
  @Column()
  public function9TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public function10: boolean;
  @Column()
  public function10TypePredictived: number;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public isActived: boolean;

  @Column()
  public webOwner: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}

export default PredictPMook;
