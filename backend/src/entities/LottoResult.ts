import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class LottoResult {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public round: number;

  @Column()
  public name: string;

  @Column()
  public lotto2cha: number;

  @Column()
  public lotto3cha: number;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}

export default LottoResult;
