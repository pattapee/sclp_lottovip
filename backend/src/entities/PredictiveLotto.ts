import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class PredictiveLotto {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public round: number;

  @Column()
  public name: string;

  @Column()
  public datapredict: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}

export default PredictiveLotto;
