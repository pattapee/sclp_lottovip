import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class Lotto {
  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column()
  public name: string;

  @Column()
  public linetoken: string;

  @Column()
  public contactHeadGroup: string;

  @Column()
  public endDate: Date;

  @Column()
  public category: string;

  @Column()
  public remark: string;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public isPredictived: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public huay: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public huayvip: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public huayspeed: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public huayspeedvip: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public ltobetyeekee: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public ltobetyeekeevip: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public ltobetspeed: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public ltobetspeedvip: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public chudjenbet: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public chudjenbetvip: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public dnabet: boolean;

  @Column("bit", {
    default: false,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public dnabetspeed: boolean;

  @Column("bit", {
    default: true,
    transformer: {
      from(value: Buffer): boolean | null {
        if (value === null) {
          return null;
        }
        return value[0] === 1;
      },
      to(value: boolean | null): Buffer | null {
        if (value === null) {
          return null;
        }
        const res = Buffer.from("1");
        res[0] = value ? 1 : 0;
        return res;
      },
    },
  })
  public isActive: boolean;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;
}

export default Lotto;
