import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import * as express from "express";
import helmet from "helmet";
import moment from "moment";
import cron from "node-cron";
import { createConnection, getConnection } from "typeorm";

import routes from "./api";

import APILineNotifyLottoVIP from "./services/LineNotify/LottoVIP";
// import APILineNotifyltobet from "./services/LineNotify/ltobet";
import APILineNotifyRuay from "./services/LineNotify/Ruay";
import ServicesLineNotify from "./services/LineNotify/ServicesLineNotify";
import logger from "./services/Logging";

import LottoRound from "./entities/LottoRound";
import LottoRoundRepository from "./repositories/LottoRoundRepository";

let lottoroundrepository: LottoRoundRepository;
const initializelottoround = () => {
  lottoroundrepository =
    getConnection().getCustomRepository(LottoRoundRepository);
};

createConnection()
  .then(async (connection) => {
    if (connection.isConnected === true) {
      dotenv.config();
      const port = process.env.SERVER_PORT;
      const app = express.default();

      app.use(cors());
      app.use(helmet());
      app.use(bodyParser.json()).use(bodyParser.urlencoded({ extended: true }));

      app.use("/v1", routes);

      app.listen(port, async () => {
        logger.info(`Server started on port ${port}`);

        await initializelottoround();
        const lottoround = await lottoroundrepository.getOne();

        if (!lottoround) {
          const round = new LottoRound();
          round.lottovip = 0;
          round.huay = 0;
          round.huayspeed = 0;
          round.ltobet = 0;
          round.ltobetspeed = 0;
          round.ruay = 0;
          round.chudjenbet = 0;
          round.chudjenbetspeed = 0;
          round.dnabet = 0;
          round.dnabetspeed = 0;
          await lottoroundrepository.save(round);
        } else {
          lottoround.lottovip = 0;
          lottoround.huay = 0;
          lottoround.huayspeed = 0;
          lottoround.ltobet = 0;
          lottoround.ltobetspeed = 0;
          lottoround.ruay = 0;
          lottoround.chudjenbet = 0;
          lottoround.chudjenbetspeed = 0;
          lottoround.dnabet = 0;
          lottoround.dnabetspeed = 0;
          await lottoroundrepository.update(lottoround.id, lottoround);
        }

        // InitalOldlength
        // fnInital
        // InitalOldlengthSpeed
        // fnInitalSpeed

        await APILineNotifyLottoVIP.InitalOldlength().catch((err) =>
          logger.error(err)
        );
        // await APILineNotifyltobet.InitalOldlength().catch((err) =>
        //   logger.error(err)
        // );
        await APILineNotifyRuay.InitalOldlength().catch((err) =>
          logger.error(err)
        );

        cron.schedule("00 05 * * *", async () => {
          ServicesLineNotify.scheduleCheckendDateToken().catch((err) =>
            logger.error(err)
          );
        });

        cron.schedule("0 0 */3 * * *", async () => {
          const currentDate = moment(new Date());
          if (currentDate.hour() <= 4 && currentDate.hour() >= 5) {
            await ServicesLineNotify.sendAlertDeadline().catch((err) =>
              logger.error(err)
            );
          }
        });

        // Run Notify Lotto All Group
        cron.schedule("40 02,17,32,47 * * * *", async () => {
          // const chanelLottoVIPfnInital = wt.createChannel(
          await APILineNotifyLottoVIP.fnInital().catch((err) =>
            logger.error(err)
          );
          await APILineNotifyRuay.fnInital().catch((err) => logger.error(err));
          // await APILineNotifyltobet.fnInital().catch((err) =>
          //   logger.error(err)
          // );
        });

        // JOB notify Start Message Every
        cron.schedule("00 06 * * *", async () => {
          await ServicesLineNotify.sendMsgStartnotify().catch((err) =>
            logger.error(err)
          );
          await ServicesLineNotify.startalert0600().catch((err) =>
            logger.error(err)
          );
        });

        // JOB notifty timeout
        cron.schedule("00,15,30,45 * * * *", async () => {
          await APILineNotifyLottoVIP.lastInital().catch((err) =>
            logger.error(err)
          );
        });
      });
    } else {
      logger.error(connection);
    }
  })
  .catch((err) => logger.error(err));
