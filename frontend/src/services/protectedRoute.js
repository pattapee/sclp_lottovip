import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import BackendAPI from "./backend";

export const ProtectedRoute = ({ children }) => {
  const usertoken = localStorage.getItem("usertoken");
  const userrole = localStorage.getItem("userrole");

  const navigate = useNavigate();

  if (!usertoken && !userrole) {
    return <Navigate to="/login" replace={true} />;
  }

  try {
    const data = {
      usertoken: usertoken,
      userrole: userrole,
    };
    const chkToken = BackendAPI()
      .post(`/checktoken`, data)
      .then(() => {
        if (chkToken) {
          return children;
        }
      })
      .catch((res) => {
        let titleError = res.response.data.data;

        if (res.response.status === 401) {
          navigate("/login");
          titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
        }
        Swal.fire({
          icon: "error",
          title: titleError,
        });
      });

    if (chkToken) {
      return children;
    }
  } catch (err) {
    console.error("🚀 ~ file: ProtectedRoute ~ err", err);
    navigate("/login");
  }
};
