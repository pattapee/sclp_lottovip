import axios from "axios";

export default function DNABetServices() {
  return axios.create({
    baseURL: `${process.env.REACT_APP_DNABET_URL}/v1`,
    timeout: 3000,
    headers: {
      "Content-Type": "application/json",
      authorization: `Bearer ${process.env.SECRETTOKEN}`,
    },
  });
}
