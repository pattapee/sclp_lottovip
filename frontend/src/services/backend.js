import axios from "axios";

export default function BackendAPI() {
  const usertoken = localStorage.getItem("usertoken");

  return axios.create({
    baseURL: `${process.env.REACT_APP_BACKEND_URL}/v1`,
    timeout: 3000,
    headers: {
      "Content-Type": "application/json",
      auth: usertoken,
    },
  });
}
