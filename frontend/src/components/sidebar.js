import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import BackendAPI from "../services/backend";
import Footer from "./footer";
import "./template.css";

export default function Sidebar() {
  const urlPathLotto = "/lotto/list";
  const userrole = localStorage.getItem("userrole");

  const [count, setCount] = useState();

  const getDashboard = async () => {
    const { data: count } = await BackendAPI().get("/count");

    if (count) {
      setCount(count);
    }
  };

  useEffect(() => {
    getDashboard();
    const interval = setInterval(() => {
      getDashboard();
    }, 60 * 1000);
    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {count && (
        <div className="fixed flex flex-col top-14 left-0 w-14 hover:w-64 md:w-64 bg-blue-900 dark:bg-gray-900 h-full text-white transition-all duration-300 border-none z-10 sidebar">
          <div className="overflow-y-auto overflow-x-hidden flex flex-col justify-between flex-grow">
            <ul className="flex flex-col py-4 space-y-1">
              <li className="px-5 hidden md:block">
                <div className="flex flex-row items-center h-8">
                  <div className="text-sm font-light tracking-wide text-gray-400 uppercase">
                    Main
                  </div>
                </div>
              </li>

              {userrole === "SUPERADMIN" && (
                <>
                  <li>
                    <Link
                      to="/dashboard"
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Dashboard
                      </span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      to={`${urlPathLotto}/lottovip`}
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Lotto VIP
                      </span>
                      <span className="hidden md:block px-2 py-0.5 ml-auto text-xs font-bold tracking-wide text-green-600 bg-red-50 rounded-full">
                        {(count.countActive && count.countActive.lottovip) || 0}
                      </span>
                      {/* &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-yellow-500 bg-red-50 rounded-full">
                        X
                      </span> */}
                      &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-red-600 bg-red-50 rounded-full">
                        {(count.countInActive &&
                          count.countInActive.lottovip) ||
                          0}
                      </span>
                    </Link>
                  </li>

                  <li>
                    <Link
                      to={`${urlPathLotto}/ltobet`}
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Ltobet
                      </span>
                      <span className="hidden md:block px-2 py-0.5 ml-auto text-xs font-bold tracking-wide text-green-600 bg-red-50 rounded-full">
                        {(count.countActive && count.countActive.ltobet) || 0}
                      </span>
                      {/* &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-yellow-500 bg-red-50 rounded-full">
                        X
                      </span> */}
                      &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-red-600 bg-red-50 rounded-full">
                        {(count.countInActive && count.countInActive.ltobet) ||
                          0}
                      </span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      to={`${urlPathLotto}/ruay`}
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Ruay
                      </span>
                      <span className="hidden md:block px-2 py-0.5 ml-auto text-xs font-bold tracking-wide text-green-600 bg-red-50 rounded-full">
                        {(count.countActive && count.countActive.ruay) || 0}
                      </span>
                      {/* &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-yellow-500 bg-red-50 rounded-full">
                        X
                      </span> */}
                      &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-red-600 bg-red-50 rounded-full">
                        {(count.countInActive && count.countInActive.ruay) || 0}
                      </span>
                    </Link>
                  </li>
                  {(userrole === "MOOK" || userrole === "SUPERADMIN") && (
                    <li>
                      <Link
                        to={`${urlPathLotto}/dnabet`}
                        className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                      >
                        <span className="inline-flex justify-center items-center ml-4">
                          <svg
                            className="w-5 h-5"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              strokeLinecap="round"
                              strokeWidth="2"
                              d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                            ></path>
                          </svg>
                        </span>
                        <span className="ml-2 text-sm tracking-wide truncate">
                          DNABet
                        </span>
                        <span className="hidden md:block px-2 py-0.5 ml-auto text-xs font-bold tracking-wide text-green-600 bg-red-50 rounded-full">
                          {(count.countActive && count.countActive.dnabet) ||
                            "-"}
                        </span>
                        {/* &nbsp;
                      <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-yellow-500 bg-red-50 rounded-full">
                        X
                      </span> */}
                        &nbsp;
                        <span className="hidden md:block px-2 py-0.5 text-xs font-bold tracking-wide text-red-600 bg-red-50 rounded-full">
                          {(count.countInActive &&
                            count.countInActive.dnabet) ||
                            0}
                        </span>
                      </Link>
                    </li>
                  )}
                </>
              )}

              {(userrole === "MOOK" || userrole === "SUPERADMIN") && (
                <>
                  <li>
                    <Link
                      to="/lotto/predict/adminmook"
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        ChudJenBet By Mook
                      </span>
                    </Link>
                  </li>
                </>
              )}
              {(userrole === "MOOK" || userrole === "SUPERADMIN") && (
                <>
                  <li className="px-5 hidden md:block">
                    <div className="flex flex-row items-center mt-5 h-8">
                      <div className="text-sm font-light tracking-wide text-gray-400 uppercase">
                        Settings
                      </div>
                    </div>
                  </li>

                  <li>
                    <Link
                      to="/user/list"
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Users
                      </span>
                    </Link>
                  </li>
                  <li>
                    <Link
                      to="/setting"
                      className="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 dark:hover:bg-gray-600 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 dark:hover:border-gray-800 pr-6"
                    >
                      <span className="inline-flex justify-center items-center ml-4">
                        <svg
                          className="w-5 h-5"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 24 24"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                          ></path>
                          <path
                            strokeLinecap="round"
                            strokeWidth="2"
                            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                          ></path>
                        </svg>
                      </span>
                      <span className="ml-2 text-sm tracking-wide truncate">
                        Settings
                      </span>
                    </Link>
                  </li>
                </>
              )}
            </ul>
            <Footer />
          </div>
        </div>
      )}
    </>
  );
}
