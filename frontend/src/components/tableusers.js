import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import orderBy from "lodash/orderBy";
import filter from "lodash/filter";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import BackendAPI from "../services/backend";
import manifest from "./manifest.json";

export default function TableUser(props) {
  const { Sort, txtSearch } = props;
  const [users, setUsers] = useState();
  const userrole = localStorage.getItem("userrole");

  const navigate = useNavigate();

  const getAllUsers = async () => {
    try {
      const { data: user } = await BackendAPI().get(`/users`);

      if (user) {
        let procressUser = orderBy(user, ["updatedAt"], [Sort]);

        if (txtSearch) {
          procressUser = filter(procressUser, (result) => {
            return (
              result.fullname.toLowerCase().indexOf(txtSearch.toLowerCase()) >
                -1 ||
              result.username.toLowerCase().indexOf(txtSearch.toLowerCase()) >
                -1
            );
          });
        }

        setUsers(procressUser);
      }
    } catch (err) {
      console.error(err);
      let titleError = err.response.data.data;

      if (err.response.status === 401) {
        navigate("/login");
        titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
      }

      await Swal.fire({
        icon: "error",
        title: titleError,
      });
    }
  };

  const onDelete = async (userID, Fullname) => {
    const ansCTADelete = await Swal.fire({
      icon: "warning",
      title: `${manifest.confirmDeleteUser}\n *** ${Fullname} ***`,
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#808080",
      confirmButtonText: manifest.Confirm,
      cancelButtonText: manifest.Cancle,
    });

    if (ansCTADelete.isConfirmed) {
      try {
        const result = await BackendAPI().delete(`/users/${userID}`);

        if (result.status === 202) {
          await getAllUsers();
        }
      } catch (err) {
        console.error(err.response.data);
        let titleError = err.response.data.data;

        if (err.response.status === 401) {
          navigate("/login");
          titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
        }

        await Swal.fire({
          icon: "error",
          title: titleError,
        });
      }
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      await getAllUsers();
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [Sort, txtSearch]);

  return (
    <>
      <table className="w-full whitespace-nowrap">
        <thead>
          <tr className="focus:outline-none h-16 border border-gray-100 rounded text-left">
            {userrole === "SUPERADMIN" && <td></td>}
            <td className="pl-5">{manifest.headerTableUserUsername}</td>
            <td className="pl-5">{manifest.headerTableUserFullname}</td>
            <td className="pl-5">{manifest.headerTableuserrole}</td>
          </tr>
        </thead>
        <tbody>
          {users &&
            users.map((user) => {
              return (
                <tr
                  key={user.id}
                  className="focus:outline-none h-12 border border-gray-100 hover:bg-gray-100 cursor-pointer"
                >
                  {userrole === "SUPERADMIN" && (
                    <>
                      <td className="w-2/12">
                        &nbsp;&nbsp;
                        <div className="inline-flex items-center rounded-md shadow-sm">
                          <Link
                            to={`/user/${user.id}`}
                            className="text-slate-800 bg-yellow-300 text-sm hover:bg-yellow-200 border border-slate-200 font-medium px-4 py-2 inline-flex space-x-1 items-center"
                          >
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-4 h-4"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeWidth="2"
                                  d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                />
                              </svg>
                            </span>
                            <span className="hidden sm:block">
                              {manifest.edit}
                            </span>
                          </Link>
                          <button
                            className="text-white bg-red-500 text-sm hover:bg-red-300 border border-slate-200 rounded-r-lg font-medium px-4 py-2 inline-flex space-x-1 items-center"
                            onClick={() => {
                              onDelete(user.id, user.fullname);
                            }}
                          >
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="currentColor"
                                className="w-4 h-4"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeWidth="2"
                                  d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                />
                              </svg>
                            </span>
                            <span className="hidden sm:block">
                              {manifest.delete}
                            </span>
                          </button>
                        </div>
                      </td>
                    </>
                  )}
                  <td className="w-3/12">
                    <div className="flex items-center pl-5">
                      <p className=" truncate break-all text-base font-medium leading-none whitespace-pre-wrap text-gray-700 mr-2">
                        {user.username}
                      </p>
                    </div>
                  </td>
                  <td className="w-3/12">
                    <div className="flex items-center pl-5">
                      <p className="truncate break-all text-base font-medium leading-none whitespace-pre-wrap text-gray-700 mr-2">
                        {user.fullname}
                      </p>
                    </div>
                  </td>
                  <td className="w-2/12">
                    <div className="flex items-center pl-5">
                      <p className="truncate break-all text-base font-medium leading-none whitespace-pre-wrap text-gray-700 mr-2">
                        {user.role}
                      </p>
                    </div>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
}
