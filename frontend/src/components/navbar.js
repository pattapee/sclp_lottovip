import React from "react";
import { Link } from "react-router-dom";

import "./template.css";

export default function Navbar() {
  const username = localStorage.getItem("username");

  return (
    <>
      <div className="fixed w-full flex items-center justify-start h-14 text-white z-10">
        <div className="flex items-center justify-start pl-3 w-full h-14 bg-blue-800 dark:bg-gray-800 border-none">
          <img
            className="w-7 h-7 md:w-10 md:h-10 mr-2 rounded-md overflow"
            alt="avatar_profile"
            src="https://therminic2018.eu/wp-content/uploads/2018/07/dummy-avatar.jpg"
          />
          <span>{username}</span>
        </div>
        <div className="flex justify-end items-center h-14 bg-blue-800 dark:bg-gray-800 ">
          <div className="flex items-center w-full max-w-xl mr-4 p-2 "></div>
          <ul className="flex items-center">
            <li>
              <Link
                to="/login"
                className="flex items-center mr-4 hover:text-blue-100"
              >
                <span className="inline-flex mr-1">
                  <svg
                    className="w-5 h-5"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeWidth="2"
                      d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
                    ></path>
                  </svg>
                  <span>Logout</span>
                </span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
