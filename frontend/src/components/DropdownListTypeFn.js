import React from "react";
import find from "lodash/find";

export default function DropdownListTypeFn(props) {
  const { name, save, defaultvalue } = props;
  let optionValue = { value: 0, name: "" };
  const listValue = [
    {
      value: 1,
      name: "เสียวตัวเดียว",
    },
    {
      value: 2,
      name: "ปักสิบบน",
    },
    {
      value: 3,
      name: "ปักหน่วยบน",
    },
    {
      value: 4,
      name: "ปักสิบล่าง",
    },
    {
      value: 5,
      name: "ปักหน่วยล่าง",
    },
  ];
  if (defaultvalue) {
    optionValue = find(listValue, { value: defaultvalue });
  }

  return (
    <>
      <div className="flex items-center -mt-2 mb-5 ml-8 w-1/3">
        <select
          id={name}
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          onChange={(e) => {
            if (e.target.value) {
              save(e.target.value);
            }
          }}
        >
          {defaultvalue && (
            <>
              <option value={optionValue.value}>{optionValue.name}</option>
            </>
          )}

          {listValue &&
            listValue.map((item) => {
              return <option value={item.value}> {item.name} </option>;
            })}
        </select>
      </div>
    </>
  );
}
