import React from "react";
import "./template.css";

export default function Footer() {
  return (
    <>
      <div>
        <p className="mb-14 px-5 py-3 hidden md:block text-center text-xs">
          Copyright @ {new Date().getFullYear()}
        </p>
      </div>
    </>
  );
}
