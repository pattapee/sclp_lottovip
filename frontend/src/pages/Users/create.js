import React from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import BackendAPI from "../../services/backend";
import manifest from "./manifest";

export default function UserCreatePage() {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();

  const onSubmit = async (result) => {
    try {
      const { data: user } = await BackendAPI().post(`/users`, result);

      if (user) {
        navigate("/user/list");
        await Swal.fire({
          icon: "success",
          title: `${manifest.txtAlertCreateUserSuccess} <br> ${user.username}`,
        });
      }
    } catch (err) {
      let titleError = err.response.data.data;

      if (err.response.status === 401) {
        navigate("/login");
        titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
      }

      await Swal.fire({
        icon: "error",
        title: titleError,
      });
    }
  };

  return (
    <>
      <div className="pl-10 w-full">
        <div className="px-4 md:px-10 py-4 md:py-7">
          <div className="flex items-center justify-between">
            <p
              tabIndex="0"
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              {manifest.txtCreateHeader}
            </p>
          </div>
          <div className="flex items-left justify-left pt-8">
            <div className="w-full max-w-[550px]">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3 sm:w-1/2">
                    <div className="mb-5">
                      <label
                        for="Username"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        Username
                      </label>
                      <input
                        type="text"
                        name="Username"
                        id="Username"
                        placeholder="Username"
                        className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        required
                        {...register("username")}
                      />
                    </div>
                  </div>
                  <div className="w-full px-3 sm:w-1/2">
                    <div className="mb-5">
                      <label
                        for="Password"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        Password
                      </label>
                      <input
                        type="password"
                        name="Password"
                        id="Password"
                        placeholder="Password"
                        className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        required
                        {...register("password")}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-5">
                  <label
                    for="fullname"
                    className="mb-3 block text-base font-medium text-[#07074D]"
                  >
                    {manifest.txtFullnameThai} - (Fullname)
                  </label>
                  <input
                    type="text"
                    name="fullname"
                    id="fullname"
                    placeholder={manifest.txtFullnameThai}
                    className="w-full appearance-none rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                    required
                    {...register("fullname")}
                  />
                </div>

                <div className="mb-5">
                  <label className="mb-3 block text-base font-medium text-[#07074D]">
                    {manifest.txtChooseRoleUser}
                  </label>
                  <div className="flex items-center space-x-6">
                    <select
                      id="role"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                      {...register("role")}
                    >
                      <option value="">Choose a role</option>
                      <option value="SUPERADMIN">SUPER ADMIN</option>
                      <option value="ADMIN">ADMIN</option>
                      <option value="MOOK">MOOK</option>
                    </select>
                  </div>
                </div>

                <div className="pt-8">
                  <button className="hover:shadow-form rounded-md bg-green-600 hover:bg-green-500 py-3 px-8 text-center text-base font-semibold text-white outline-none ">
                    <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 20 20"
                          className="w-5 h-5"
                        >
                          <path d="M17.064,4.656l-2.05-2.035C14.936,2.544,14.831,2.5,14.721,2.5H3.854c-0.229,0-0.417,0.188-0.417,0.417v14.167c0,0.229,0.188,0.417,0.417,0.417h12.917c0.229,0,0.416-0.188,0.416-0.417V4.952C17.188,4.84,17.144,4.733,17.064,4.656M6.354,3.333h7.917V10H6.354V3.333z M16.354,16.667H4.271V3.333h1.25v7.083c0,0.229,0.188,0.417,0.417,0.417h8.75c0.229,0,0.416-0.188,0.416-0.417V3.886l1.25,1.239V16.667z M13.402,4.688v3.958c0,0.229-0.186,0.417-0.417,0.417c-0.229,0-0.417-0.188-0.417-0.417V4.688c0-0.229,0.188-0.417,0.417-0.417C13.217,4.271,13.402,4.458,13.402,4.688" />
                        </svg>
                      </span>
                      &nbsp;
                      {manifest.confirm}
                    </p>
                  </button>
                  &nbsp;&nbsp;&nbsp;
                  <Link to="/user/list">
                    <button className="hover:shadow-form rounded-md bg-gray-400 hover:bg-gray-300 py-3 px-8 text-center text-base font-semibold text-white outline-none">
                      <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                        <span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 20 20"
                            className="w-5 h-5"
                          >
                            <path d="M3.24,7.51c-0.146,0.142-0.146,0.381,0,0.523l5.199,5.193c0.234,0.238,0.633,0.064,0.633-0.262v-2.634c0.105-0.007,0.212-0.011,0.321-0.011c2.373,0,4.302,1.91,4.302,4.258c0,0.957-0.33,1.809-1.008,2.602c-0.259,0.307,0.084,0.762,0.451,0.572c2.336-1.195,3.73-3.408,3.73-5.924c0-3.741-3.103-6.783-6.916-6.783c-0.307,0-0.615,0.028-0.881,0.063V2.575c0-0.327-0.398-0.5-0.633-0.261L3.24,7.51 M4.027,7.771l4.301-4.3v2.073c0,0.232,0.21,0.409,0.441,0.366c0.298-0.056,0.746-0.123,1.184-0.123c3.402,0,6.172,2.709,6.172,6.041c0,1.695-0.718,3.24-1.979,4.352c0.193-0.51,0.293-1.045,0.293-1.602c0-2.76-2.266-5-5.046-5c-0.256,0-0.528,0.018-0.747,0.05C8.465,9.653,8.328,9.81,8.328,9.995v2.074L4.027,7.771z" />
                          </svg>
                        </span>
                        &nbsp;
                        {manifest.txtBack}
                      </p>
                    </button>
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
