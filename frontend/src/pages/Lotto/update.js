import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import BackendAPI from "../../services/backend";
import manifest from "./manifest";

export default function LottoUpdatePage() {
  const { register, handleSubmit } = useForm();
  const { id } = useParams();
  const navigate = useNavigate();
  const [lotto, setLotto] = useState();
  const [category, setCategory] = useState();
  const [endDate, setEndDate] = useState();

  const prepareData = async () => {
    try {
      const { data: lotto } = await BackendAPI().get(`/lotto/${id}`);

      if (lotto) {
        setLotto(lotto);
        setEndDate(new Date(lotto.endDate));
        setCategory(lotto.category);
      }
    } catch (err) {
      let titleError = err.response.data.data;

      if (err.response.status === 401) {
        navigate("/login");
        titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
      }

      await Swal.fire({
        icon: "error",
        title: titleError,
      });
    }
  };

  const onSubmit = async (result) => {
    result.endDate = endDate;
    result.isPredictived = lotto.isPredictived;

    try {
      const { data: lotto } = await BackendAPI().patch(`/lotto`, result);

      if (lotto) {
        navigate(`/lotto/list/${category}`);
        await Swal.fire({
          icon: "success",
          title: `${manifest.txtAlertCreateLottoSuccess} <br> ${lotto.name}`,
        });
      }
    } catch (err) {
      let titleError = err.response.data.data;
      if (err.response.status === 401) {
        navigate("/login");
        titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
      }
      await Swal.fire({
        icon: "error",
        title: titleError,
      });
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      await prepareData();
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <>
      <div className="pl-10 w-full">
        <div className="px-4 md:px-10 py-4 md:py-7">
          <div className="flex items-center justify-between">
            <p
              tabIndex="0"
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              {manifest.txtUpdateHeader}
            </p>
          </div>
          <div className="flex items-left justify-left pt-8">
            <div className="w-full max-w-[550px]">
              {lotto && (
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3">
                      <div className="mb-5">
                        <label
                          htmlFor="id"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          ID
                        </label>
                        <input
                          type="text"
                          name="id"
                          id="id"
                          className="w-full rounded-md border border-[#e0e0e0] bg-gray-300 py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                          required
                          readOnly
                          defaultValue={lotto.id}
                          {...register("id")}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3">
                      <div className="mb-5">
                        <label
                          htmlFor="linetoken"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          Token Line Notify
                        </label>
                        <input
                          type="text"
                          name="linetoken"
                          id="linetoken"
                          placeholder="ระบุ Token"
                          className="w-full rounded-md border border-[#e0e0e0] bg-gray-100 py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                          required
                          defaultValue={lotto.linetoken}
                          {...register("linetoken")}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3 sm:w-1/2">
                      <div className="mb-5">
                        <label
                          htmlFor="nameGroup"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          {manifest.nameGroup}
                        </label>
                        <input
                          type="text"
                          name="nameGroup"
                          id="nameGroup"
                          placeholder="ระบุ ชื่อกลุ่ม"
                          className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                          required
                          defaultValue={lotto.name}
                          {...register("name")}
                        />
                      </div>
                    </div>
                    <div className="w-full px-3 sm:w-1/2">
                      <div className="mb-5">
                        <label
                          htmlFor="contactHeadGroup"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          {manifest.contactHeadGroup}
                        </label>
                        <input
                          type="text"
                          name="contactHeadGroup"
                          id="contactHeadGroup"
                          placeholder="ระบุ ชื่อผู้ดูแลกลุ่ม"
                          className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                          required
                          defaultValue={lotto.contactHeadGroup}
                          {...register("contactHeadGroup")}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3 ">
                      <div className="mb-5">
                        <label className="mb-3 block text-base font-medium text-[#07074D]">
                          ประเภทเว็บ อาทิเช่น <br />
                          <span className="text-sm">
                            [HUAY, LOTTOVIP, LTOBET, RUAY, CHUDJENBET]
                          </span>
                        </label>
                        <div className="flex items-center space-x-6 my-4">
                          <select
                            id="category"
                            className="bg-gray-300 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                          >
                            <option value="">{lotto.category}</option>
                          </select>
                        </div>

                        {lotto.category === "lottovip" && (
                          <>
                            <div className="flex items-center mb-5 ml-5">
                              <input
                                id="ChkisPredictived"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                                defaultChecked={lotto.isPredictived}
                                {...register("isPredictived")}
                              />
                              <label
                                htmlFor="txtisPredictived"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                บอทนำเล่น Lotto VIP
                              </label>
                            </div>
                          </>
                        )}

                        {lotto.category === "huay" && (
                          <>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkhuay"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.huay}
                                disabled
                              />
                              <label
                                htmlFor="txtchkhuay"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Huay
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkhuayvip"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.huayvip}
                                disabled
                              />
                              <label
                                htmlFor="txtchkhuayvip"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Huay VIP
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkhuayspeed"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.huayspeed}
                                disabled
                              />
                              <label
                                htmlFor="txtchkhuayspeed"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Huay Speed 5 นาที
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkhuayspeedvip"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.huayspeedvip}
                                disabled
                              />
                              <label
                                htmlFor="txtchkhuayspeedvip"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Huay VIP Speed 5 นาที
                              </label>
                            </div>
                          </>
                        )}

                        {lotto.category === "ltobet" && (
                          <>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkltobetyeekee"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                {...register("ltobetyeekee")}
                                defaultChecked={lotto.ltobetyeekee}
                                disabled
                              />
                              <label
                                htmlFor="txtchkltobetyeekee"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Ltobet Yeekee
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkltobetyeekeevip"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                {...register("ltobetyeekeevip")}
                                defaultChecked={lotto.ltobetyeekeevip}
                                disabled
                              />
                              <label
                                htmlFor="txtchkltobetyeekeevip"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Ltobet YeeKee VIP
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkltobetyeekeespeed"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                {...register("ltobetyeekeespeed")}
                                defaultChecked={lotto.ltobetspeed}
                                disabled
                              />
                              <label
                                htmlFor="txtchkltobetyeekeespeed"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Ltobet Yeekee 5 นาที
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chkltobetyeekeespeedvip"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                {...register("ltobetyeekeespeedvip")}
                                defaultChecked={lotto.ltobetspeedvip}
                                disabled
                              />
                              <label
                                htmlFor="txtchkltobetyeekeespeedvip"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                Ltobet Yeekee VIP 5 นาที
                              </label>
                            </div>
                          </>
                        )}

                        {lotto.category === "chudjenbet" && (
                          <>
                            <div className="flex items-center mb-5">
                              <input
                                id="chudjenbet"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.chudjenbet}
                                disabled
                              />
                              <label
                                htmlFor="chudjenbet"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                ยี่กี่ มีโชค
                              </label>
                            </div>
                            <div className="flex items-center mb-5">
                              <input
                                id="chudjenbetvip"
                                type="checkbox"
                                className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-4 w-4 rounded"
                                defaultChecked={lotto.chudjenbetvip}
                                disabled
                              />
                              <label
                                htmlFor="chudjenbetvip"
                                className="text-sm ml-3 font-medium text-gray-900"
                              >
                                ยี่กี่ มีลาภ (Speed 5 นาที)
                              </label>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3 sm:w-1/2">
                      <div className="mb-5">
                        <label
                          htmlFor="startDate"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          {manifest.startDate}
                        </label>
                      </div>
                    </div>
                    <div className="w-full px-3 sm:w-1/2">
                      <div className="mb-5">
                        <label
                          htmlFor="contactHeadGroup"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          {manifest.endDate}
                        </label>
                        <DatePicker
                          selected={endDate}
                          onChange={(date) => setEndDate(date)}
                          className="w-full border-2 py-3 px-6 border-[#e0e0e0] rounded-md text-base font-medium text-[#6B7280]"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="-mx-3 flex flex-wrap">
                    <div className="w-full px-3">
                      <div className="mb-5">
                        <label
                          htmlFor="remark"
                          className="mb-3 block text-base font-medium text-[#07074D]"
                        >
                          {manifest.remark}
                        </label>
                        <textarea
                          id="remark"
                          name="remark"
                          rows="4"
                          cols="50"
                          placeholder="ระบุ หมายเหตุ"
                          defaultValue={lotto.remark}
                          className="w-full rounded-md border-2 py-3 px-6 border-[#e0e0e0]"
                          {...register("remark")}
                        ></textarea>
                      </div>
                    </div>
                  </div>

                  <div className="pt-8">
                    <button className="hover:shadow-form rounded-md bg-green-600 hover:bg-green-500 py-3 px-8 text-center text-base font-semibold text-white outline-none ">
                      <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                        <span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 20 20"
                            className="w-5 h-5"
                          >
                            <path d="M17.064,4.656l-2.05-2.035C14.936,2.544,14.831,2.5,14.721,2.5H3.854c-0.229,0-0.417,0.188-0.417,0.417v14.167c0,0.229,0.188,0.417,0.417,0.417h12.917c0.229,0,0.416-0.188,0.416-0.417V4.952C17.188,4.84,17.144,4.733,17.064,4.656M6.354,3.333h7.917V10H6.354V3.333z M16.354,16.667H4.271V3.333h1.25v7.083c0,0.229,0.188,0.417,0.417,0.417h8.75c0.229,0,0.416-0.188,0.416-0.417V3.886l1.25,1.239V16.667z M13.402,4.688v3.958c0,0.229-0.186,0.417-0.417,0.417c-0.229,0-0.417-0.188-0.417-0.417V4.688c0-0.229,0.188-0.417,0.417-0.417C13.217,4.271,13.402,4.458,13.402,4.688" />
                          </svg>
                        </span>
                        &nbsp;
                        {manifest.confirm}
                      </p>
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <Link to="/dashboard">
                      <button className="hover:shadow-form rounded-md bg-gray-400 hover:bg-gray-300 py-3 px-8 text-center text-base font-semibold text-white outline-none">
                        <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                          <span>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              stroke="currentColor"
                              viewBox="0 0 20 20"
                              className="w-5 h-5"
                            >
                              <path d="M3.24,7.51c-0.146,0.142-0.146,0.381,0,0.523l5.199,5.193c0.234,0.238,0.633,0.064,0.633-0.262v-2.634c0.105-0.007,0.212-0.011,0.321-0.011c2.373,0,4.302,1.91,4.302,4.258c0,0.957-0.33,1.809-1.008,2.602c-0.259,0.307,0.084,0.762,0.451,0.572c2.336-1.195,3.73-3.408,3.73-5.924c0-3.741-3.103-6.783-6.916-6.783c-0.307,0-0.615,0.028-0.881,0.063V2.575c0-0.327-0.398-0.5-0.633-0.261L3.24,7.51 M4.027,7.771l4.301-4.3v2.073c0,0.232,0.21,0.409,0.441,0.366c0.298-0.056,0.746-0.123,1.184-0.123c3.402,0,6.172,2.709,6.172,6.041c0,1.695-0.718,3.24-1.979,4.352c0.193-0.51,0.293-1.045,0.293-1.602c0-2.76-2.266-5-5.046-5c-0.256,0-0.528,0.018-0.747,0.05C8.465,9.653,8.328,9.81,8.328,9.995v2.074L4.027,7.771z" />
                            </svg>
                          </span>
                          &nbsp;
                          {manifest.txtBack}
                        </p>
                      </button>
                    </Link>
                  </div>
                </form>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
