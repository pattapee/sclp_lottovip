import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import TablePMook from "../../components/tablepmook";
import setFormatDate from "../Util/setFormatDate";
import manifest from "./manifest";

import "./index.css";

export default function ChudJenBetMookPage() {
  const [sort, setSort] = useState("desc");
  const [search, setSearch] = useState("");
  const [txtSearch, setTxtSearch] = useState("");

  useEffect(() => {}, []);

  return (
    <>
      <div className="pl-10 w-full">
        <div className="px-4 md:px-10 py-4 md:py-7">
          <div className="flex items-center justify-between">
            <p
              tabIndex="0"
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              {manifest.txtHeader}
            </p>
            <div className="py-3 px-4 flex items-center text-sm font-medium leading-none text-gray-600 bg-gray-200 hover:bg-gray-300 cursor-pointer rounded">
              <p>{manifest.sortBy}:</p>
              <select
                aria-label="select"
                className="focus:text-indigo-600 focus:outline-none bg-transparent ml-1"
                onChange={(value) => {
                  setSort(value.target.value);
                }}
              >
                <option
                  className="text-sm text-indigo-800"
                  key="desc"
                  value="desc"
                >
                  {manifest.Latest}
                </option>
                <option
                  className="text-sm text-indigo-800"
                  value="asc"
                  key="asc"
                >
                  {manifest.Oldest}
                </option>
              </select>
            </div>
          </div>
          <span className="underline">
            {manifest.txtDataAtDate} {setFormatDate(new Date())}
          </span>
        </div>
        <div className="bg-white py-4 md:py-7 px-4 md:px-8 xl:px-10">
          <div className="sm:flex items-center justify-start">
            <Link
              to="/lotto/predict/adminmook/create"
              className="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-600 mt-4 sm:mt-0 inline-flex items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded "
            >
              <button className="text-sm font-medium leading-none text-white items-center text-center flex">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 20 20"
                  className="w-5 h-5"
                >
                  <path d="M14.613,10c0,0.23-0.188,0.419-0.419,0.419H10.42v3.774c0,0.23-0.189,0.42-0.42,0.42s-0.419-0.189-0.419-0.42v-3.774H5.806c-0.23,0-0.419-0.189-0.419-0.419s0.189-0.419,0.419-0.419h3.775V5.806c0-0.23,0.189-0.419,0.419-0.419s0.42,0.189,0.42,0.419v3.775h3.774C14.425,9.581,14.613,9.77,14.613,10 M17.969,10c0,4.401-3.567,7.969-7.969,7.969c-4.402,0-7.969-3.567-7.969-7.969c0-4.402,3.567-7.969,7.969-7.969C14.401,2.031,17.969,5.598,17.969,10 M17.13,10c0-3.932-3.198-7.13-7.13-7.13S2.87,6.068,2.87,10c0,3.933,3.198,7.13,7.13,7.13S17.13,13.933,17.13,10" />
                </svg>
                &nbsp;
                {manifest.CTACreateLotto}
              </button>
            </Link>
          </div>
          <div className="sm:flex items-center justify-between">
            <div className="py-4">
              <label htmlFor="table-search" className="sr-only">
                Search
              </label>
              <div className="relative mt-1 inline-flex">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg
                    className="w-5 h-5 text-gray-500 dark:text-gray-400"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                      clipRule="evenodd"
                    />
                  </svg>
                </div>
                <input
                  type="text"
                  id="txtSearch"
                  className="sm:w-80 lg:w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder={manifest.searchUsers}
                  value={search}
                  onChange={(event) => {
                    const textResult = event.target.value;
                    setSearch(textResult);
                    if (textResult.length >= 2) {
                      setTxtSearch(textResult);
                    } else {
                      setTxtSearch("");
                    }
                  }}
                />
              </div>
              &nbsp;&nbsp;
              <button
                onClick={() => {
                  if (search && search.length > 0) {
                    setTxtSearch("");
                    setSearch("");
                  }
                }}
                className="text-white text-sm bg-orange-600 hover:bg-orange-400 border border-slate-200 rounded-lg font-medium px-3 py-2 inline-flex space-x-1 align-middle"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-4 h-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeWidth="2"
                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                  />
                </svg>
                <span className="hidden sm:block"> {manifest.CTAClear}</span>
              </button>
            </div>
          </div>
          <div className="mt-4 overflow-x-auto">
            <TablePMook Sort={sort} txtSearch={txtSearch} />
          </div>
        </div>
      </div>
    </>
  );
}
