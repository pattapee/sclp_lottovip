import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import DropdownListTypeFn from "../../components/DropdownListTypeFn";
import BackendAPI from "../../services/backend";
import manifest from "./manifest";

export default function ChudJenBetMookCreatePage() {
  const { register, handleSubmit } = useForm();
  const [allValues, setAllValues] = useState({
    DDLfn1: false,
    function1TypePredictived: 1,
    DDLfn2: false,
    function2TypePredictived: 1,
    DDLfn3: false,
    function3TypePredictived: 1,
    DDLfn4: false,
    function4TypePredictived: 1,
    DDLfn5: false,
    function5TypePredictived: 1,
    DDLfn6: false,
    function6TypePredictived: 2,
    DDLfn7: false,
    function7TypePredictived: 2,
    DDLfn8: false,
    function8TypePredictived: 2,
    DDLfn9: false,
    function9TypePredictived: 2,
    DDLfn10: false,
    function10TypePredictived: 2,
    webOwner: "",
  });
  const [endDate, setEndDate] = useState(
    new Date(Date.now() + 3600 * 1000 * 72)
  );
  const navigate = useNavigate();

  const onSubmit = async (result) => {
    const dataLotto = {
      name: result.name,
      linetoken: result.linetoken,
      contactHeadGroup: "@Mook",
      endDate: endDate,
      remark: result.remark || " ",
      function1: result.function1 ? true : false,
      function2: result.function2 ? true : false,
      function3: result.function3 ? true : false,
      function4: result.function4 ? true : false,
      function5: result.function5 ? true : false,
      function6: result.function6 ? true : false,
      function7: result.function7 ? true : false,
      function8: result.function8 ? true : false,
      function9: result.function9 ? true : false,
      function10: result.function10 ? true : false,
      function1TypePredictived: Number(allValues.function1TypePredictived),
      function2TypePredictived: Number(allValues.function2TypePredictived),
      function3TypePredictived: Number(allValues.function3TypePredictived),
      function4TypePredictived: Number(allValues.function4TypePredictived),
      function5TypePredictived: Number(allValues.function5TypePredictived),
      function6TypePredictived: Number(allValues.function6TypePredictived),
      function7TypePredictived: Number(allValues.function7TypePredictived),
      function8TypePredictived: Number(allValues.function8TypePredictived),
      function9TypePredictived: Number(allValues.function9TypePredictived),
      function10TypePredictived: Number(allValues.function10TypePredictived),
      webOwner: result.webOwner,
    };

    try {
      const { data: lotto } = await BackendAPI().post(
        `/predictpmook`,
        dataLotto
      );

      if (lotto) {
        navigate(`/lotto/predict/adminmook`);
        await Swal.fire({
          icon: "success",
          title: `${manifest.txtAlertCreateLottoSuccess} <br> ${lotto.name}`,
        });
      }
    } catch (err) {
      let titleError = err.response.data.data;

      if (err.response.status === 401) {
        navigate("/login");
        titleError = "กรุณาลงชื่อเข้าใช้ก่อนใช้งานระบบ";
      }

      await Swal.fire({
        icon: "error",
        title: titleError,
      });
    }
  };

  return (
    <>
      <div className="pl-10 w-full">
        <div className="px-4 md:px-10 py-4 md:py-7">
          <div className="flex items-center justify-between">
            <p
              tabIndex="0"
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              {manifest.txtCreateHeader}
            </p>
          </div>
          <div className="flex items-left justify-left pt-8">
            <div className="w-full max-w-[550px]">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3">
                    <div className="mb-5">
                      <label
                        htmlFor="linetoken"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        Token Line Notify
                      </label>
                      <input
                        type="text"
                        name="linetoken"
                        id="linetoken"
                        placeholder="ระบุ Token"
                        className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        required
                        {...register("linetoken")}
                      />
                    </div>
                  </div>
                </div>
                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3">
                    <div className="mb-5">
                      <label
                        htmlFor="nameGroup"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        {manifest.nameGroup}
                      </label>
                      <input
                        type="text"
                        name="nameGroup"
                        id="nameGroup"
                        placeholder="ระบุ ชื่อกลุ่ม"
                        className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        required
                        {...register("name")}
                      />
                    </div>
                  </div>
                </div>

                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3 ">
                    <div className="mb-5">
                      <label className="mb-3 block text-base font-medium text-[#07074D]">
                        เลือกสูตรสำหรับใช้งาน
                      </label>
                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn1"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn1: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function1")}
                        />
                        <label
                          htmlFor="CHKfn1"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 1 สิบล่าง+6 (C1)
                        </label>
                      </div>
                      {allValues.DDLfn1 && (
                        <DropdownListTypeFn
                          name="DDLfunction1"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function1TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn2"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn2: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function2")}
                        />
                        <label
                          htmlFor="CHKfn2"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 2 หน่วยบน+สิบล่าง+5 (C2)
                        </label>
                      </div>
                      {allValues.DDLfn2 && (
                        <DropdownListTypeFn
                          name="DDLfunction2"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function2TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn3"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn3: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function3")}
                        />
                        <label
                          htmlFor="CHKfn3"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 3 สิบล่าง+4 (C3)
                        </label>
                      </div>
                      {allValues.DDLfn3 && (
                        <DropdownListTypeFn
                          name="DDLfunction3"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function3TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn4"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn4: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function4")}
                        />
                        <label
                          htmlFor="CHKfn4"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 4 หน่วยบน+4 (C4)
                        </label>
                      </div>
                      {allValues.DDLfn4 && (
                        <DropdownListTypeFn
                          name="DDLfunction4"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function4TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn5"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn5: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function5")}
                        />
                        <label
                          htmlFor="CHKfn5"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 5 สิบบน+7 (C5)
                        </label>
                      </div>
                      {allValues.DDLfn5 && (
                        <DropdownListTypeFn
                          name="DDLfunction5"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function5TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn6"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn6: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function6")}
                        />
                        <label
                          htmlFor="CHKfn6"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 6 ร้อย + หน่วยล่าง (C6)
                        </label>
                      </div>
                      {allValues.DDLfn6 && (
                        <DropdownListTypeFn
                          name="DDLfunction6"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function6TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn7"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn7: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function7")}
                        />
                        <label
                          htmlFor="CHKfn7"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 7 สิบล่าง+สิบล่าง (C7)
                        </label>
                      </div>
                      {allValues.DDLfn7 && (
                        <DropdownListTypeFn
                          name="DDLfunction7"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function7TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn8"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn8: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function8")}
                        />
                        <label
                          htmlFor="CHKfn8"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 8 สิบล่าง+1 (C8)
                        </label>
                      </div>
                      {allValues.DDLfn8 && (
                        <DropdownListTypeFn
                          name="DDLfunction8"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function8TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn9"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn9: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function9")}
                        />
                        <label
                          htmlFor="CHKfn9"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 9 ร้อย+หน่วยบน+4 (C9)
                        </label>
                      </div>
                      {allValues.DDLfn9 && (
                        <DropdownListTypeFn
                          name="DDLfunction9"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function9TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}

                      <div className="flex items-center mb-5">
                        <input
                          id="CHKfn10"
                          type="checkbox"
                          onClick={(e) => {
                            setAllValues({
                              ...allValues,
                              DDLfn10: e.target.checked,
                            });
                          }}
                          className="bg-gray-50 border-gray-300 focus:ring-3 focus:ring-blue-300 h-6 w-6 rounded"
                          {...register("function10")}
                        />
                        <label
                          htmlFor="CHKfn10"
                          className="text-sm ml-3 font-medium text-gray-900"
                        >
                          สูตรที่ 10 สิบบน+8 (C10)
                        </label>
                      </div>
                      {allValues.DDLfn10 && (
                        <DropdownListTypeFn
                          name="DDLfunction10"
                          save={(event) => {
                            setAllValues({
                              ...allValues,
                              function10TypePredictived: event,
                            });
                          }}
                          defaultvalue={undefined}
                        />
                      )}
                    </div>
                  </div>
                </div>

                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3 sm:w-1/2">
                    <div className="mb-5">
                      <label
                        htmlFor="webOwner"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        WEB
                      </label>
                      <select
                        id="webOwner"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        {...register("webOwner")}
                        required
                      >
                        <option value="DNABet" selected>
                          DNABet
                        </option>
                        <option value="Chudjenbet">Chudjenbet</option>
                      </select>
                    </div>
                  </div>
                  <div className="w-full px-3 sm:w-1/2">
                    <div className="mb-5">
                      <label
                        htmlFor="contactHeadGroup"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        {manifest.endDate}
                      </label>
                      <DatePicker
                        selected={endDate}
                        onChange={(date) => setEndDate(date)}
                        className="w-full border-2 py-3 px-6 border-[#e0e0e0] rounded-md text-base font-medium text-[#6B7280]"
                      />
                    </div>
                  </div>
                </div>

                <div className="-mx-3 flex flex-wrap">
                  <div className="w-full px-3">
                    <div className="mb-5">
                      <label
                        htmlFor="remark"
                        className="mb-3 block text-base font-medium text-[#07074D]"
                      >
                        หมายเหตุ
                      </label>
                      <textarea
                        id="remark"
                        name="remark"
                        rows="4"
                        cols="50"
                        placeholder="ระบุ หมายเหตุ"
                        className="w-full rounded-md border-2 py-3 px-6 border-[#e0e0e0]"
                        {...register("remark")}
                      ></textarea>
                    </div>
                  </div>
                </div>

                <div className="pt-8">
                  <button className="hover:shadow-form rounded-md bg-green-600 hover:bg-green-500 py-3 px-8 text-center text-base font-semibold text-white outline-none ">
                    <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          stroke="currentColor"
                          viewBox="0 0 20 20"
                          className="w-5 h-5"
                        >
                          <path d="M17.064,4.656l-2.05-2.035C14.936,2.544,14.831,2.5,14.721,2.5H3.854c-0.229,0-0.417,0.188-0.417,0.417v14.167c0,0.229,0.188,0.417,0.417,0.417h12.917c0.229,0,0.416-0.188,0.416-0.417V4.952C17.188,4.84,17.144,4.733,17.064,4.656M6.354,3.333h7.917V10H6.354V3.333z M16.354,16.667H4.271V3.333h1.25v7.083c0,0.229,0.188,0.417,0.417,0.417h8.75c0.229,0,0.416-0.188,0.416-0.417V3.886l1.25,1.239V16.667z M13.402,4.688v3.958c0,0.229-0.186,0.417-0.417,0.417c-0.229,0-0.417-0.188-0.417-0.417V4.688c0-0.229,0.188-0.417,0.417-0.417C13.217,4.271,13.402,4.458,13.402,4.688" />
                        </svg>
                      </span>
                      &nbsp;
                      {manifest.confirm}
                    </p>
                  </button>
                  &nbsp;&nbsp;&nbsp;
                  <Link to="/lotto/predict/adminmook">
                    <button className="hover:shadow-form rounded-md bg-gray-400 hover:bg-gray-300 py-3 px-8 text-center text-base font-semibold text-white outline-none">
                      <p className="text-sm font-medium leading-none text-white items-center text-center flex">
                        <span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 20 20"
                            className="w-5 h-5"
                          >
                            <path d="M3.24,7.51c-0.146,0.142-0.146,0.381,0,0.523l5.199,5.193c0.234,0.238,0.633,0.064,0.633-0.262v-2.634c0.105-0.007,0.212-0.011,0.321-0.011c2.373,0,4.302,1.91,4.302,4.258c0,0.957-0.33,1.809-1.008,2.602c-0.259,0.307,0.084,0.762,0.451,0.572c2.336-1.195,3.73-3.408,3.73-5.924c0-3.741-3.103-6.783-6.916-6.783c-0.307,0-0.615,0.028-0.881,0.063V2.575c0-0.327-0.398-0.5-0.633-0.261L3.24,7.51 M4.027,7.771l4.301-4.3v2.073c0,0.232,0.21,0.409,0.441,0.366c0.298-0.056,0.746-0.123,1.184-0.123c3.402,0,6.172,2.709,6.172,6.041c0,1.695-0.718,3.24-1.979,4.352c0.193-0.51,0.293-1.045,0.293-1.602c0-2.76-2.266-5-5.046-5c-0.256,0-0.528,0.018-0.747,0.05C8.465,9.653,8.328,9.81,8.328,9.995v2.074L4.027,7.771z" />
                          </svg>
                        </span>
                        &nbsp;
                        {manifest.txtBack}
                      </p>
                    </button>
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
