import moment from "moment";

export default function setFormatDate(date) {
  return moment(date).add(543, "Y").format("DD-MMM-YYYY H:mm");
}
