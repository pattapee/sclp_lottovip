import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import BackendAPI from "../../services/backend";
import manifest from "./manifest.json";

export default function LoginPage() {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = () => {
      localStorage.clear();
    };
    fetchData();
  });

  const onSubmit = async (result) => {
    try {
      const { data: user } = await BackendAPI().post(`/login`, result);

      if (user) {
        localStorage.setItem("usertoken", user.token);
        localStorage.setItem("userrole", user.role);
        localStorage.setItem("username", user.username);

        if (user.role === "MOOK") {
          navigate("/lotto/predict/adminmook");
        } else {
          navigate("/dashboard");
        }
      }
    } catch (err) {
      localStorage.clear();

      await Swal.fire({
        icon: "error",
        title:
          err.code === "ERR_BAD_REQUEST"
            ? manifest.txtErrInvalidPassword
            : manifest.txtErrCantConnectDB,
      });
    }
  };

  return (
    <>
      <div className="mx-auto my-36 flex h-[300px] w-[350px] flex-col border-2 bg-white text-black shadow-xl">
        <div className="mx-8 mt-7 mb-1 flex flex-row justify-start space-x-2">
          <div className="h-7 w-3 bg-[#0DE6AC]"></div>
          <div className="w-fit text-center font-sans text-xl font-bold">
            <h1>{manifest.Login}</h1>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-col items-center">
            <input
              className="my-2 w-72 border p-2"
              type="text"
              id="username"
              placeholder={manifest.placeholderUsername}
              required
              {...register("username")}
            />
            <input
              className="my-2 w-72 border p-2"
              type="password"
              id="password"
              placeholder={manifest.placeholderPassword}
              required
              {...register("password")}
            />
          </div>
          <div className="my-2 flex justify-center">
            <input
              type="submit"
              value={manifest.Confirm}
              className="w-72 border bg-[#0DE6AC] p-2 font-sans cursor-pointer"
            />
          </div>
        </form>
      </div>
    </>
  );
}
