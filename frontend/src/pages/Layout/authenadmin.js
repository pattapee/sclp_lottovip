import React from "react";
import Navbar from "../../components/navbar";
import Sidebar from "../../components/sidebar";

export default function AuthenAdminPage({ children }) {
  return (
    <>
      <div>
        <div className="min-h-screen flex flex-col flex-shrink-0 antialiased bg-white dark:bg-gray-700 text-black dark:text-white">
          <Navbar />
          <Sidebar />
          <div className="h-full ml-4 mt-14 mb-10 md:ml-64">{children}</div>
        </div>
      </div>
    </>
  );
}
