import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";

import BackendAPI from "../../services/backend";
import DNABetServices from "../../services/DNABetServices";
import setFormatDate from "../Util/setFormatDate";
import manifest from "./manifest.json";

export default function SettingPage() {
  const [roundLotto, setRoundLotto] = useState();

  const getRoundLotto = async () => {
    try {
      const { data: roundLotto } = await BackendAPI().get(`/round`);

      if (roundLotto) {
        setRoundLotto(roundLotto);
      }
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      await getRoundLotto();
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const triggerResetLotto = async (group) => {
    const result = await Swal.fire({
      icon: "warning",
      title: "ยืนยันการล้างค่า \nและส่งรายงานผลใหม่ !",
      showCancelButton: true,
      confirmButtonText: manifest.txtConfirm,
      confirmButtonColor: "Red",
      cancelButtonText: manifest.txtCancle,
    });
    if (result.isConfirmed === true) {
      let ans = null;

      if (group === "dnabetspeed" || group === "chudjenbetspeed") {
        ans = await DNABetServices().get(`/lotto/trigger/${group}`);
      } else {
        ans = await BackendAPI().get(`/lotto/trigger/${group}`);
      }

      await Swal.fire("ดำเนินการล้างค่าแล้ว!", "", "success");
      if (ans.data) await getRoundLotto();
    }
  };

  const renderRestartRoundLottoBox = (txtHeader, txtoldlength, triggerName) => {
    return (
      <article className=" bg-white shadow-2xl rounded-2xl p-5 text-center grid justify-center">
        <h1 className="font-bold text-2xl text-blue-600">{txtHeader}</h1>
        <h3 className="font-bold text-1xl text-gray-700 mt-4 mb-6">
          {manifest.txtReportRoundCurrent}
          <span className="text-3xl text-red-600 underline">
            {txtoldlength}
          </span>
        </h3>
        <button
          href="#"
          className="rounded-lg py-3 px-10 text-center text-white bg-red-600 hover:bg-red-400"
          onClick={() => {
            triggerResetLotto(triggerName);
          }}
        >
          RESET
        </button>
      </article>
    );
  };

  return (
    <>
      <div className="pl-10 w-full">
        <div className="px-4 md:px-10 py-4 md:py-7">
          <div className="flex items-center justify-left">
            <p
              tabIndex="0"
              className="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800"
            >
              {manifest.txtHeader}
            </p>
          </div>
          <span className="underline">
            {manifest.txtDataAtDate} {setFormatDate(new Date())}
          </span>
        </div>
        <div className="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 grid-cols-1 gap-6 m-10 mb-10">
          {roundLotto &&
            roundLotto.oldlengthLottoVIP >= 0 &&
            renderRestartRoundLottoBox(
              "LOTTO VIP",
              roundLotto.oldlengthLottoVIP,
              "lottovip"
            )}

          {roundLotto &&
            roundLotto.oldlengthLtobet >= 0 &&
            renderRestartRoundLottoBox(
              "LTOBET",
              roundLotto.oldlengthLtobet,
              "ltobet"
            )}
          {roundLotto &&
            roundLotto.oldlengthRuay >= 0 &&
            renderRestartRoundLottoBox(
              "RUAY",
              roundLotto.oldlengthRuay,
              "ruay"
            )}
          {roundLotto &&
            roundLotto.oldlengthChudjenbetSpeed >= 0 &&
            renderRestartRoundLottoBox(
              "CHUDJENBET_SPEED",
              roundLotto.oldlengthChudjenbetSpeed,
              "chudjenbetspeed"
            )}
          {roundLotto &&
            roundLotto.oldlengthDNABetSpeed >= 0 &&
            renderRestartRoundLottoBox(
              "DNABet_SPEED",
              roundLotto.oldlengthDNABetSpeed,
              "dnabetspeed"
            )}
        </div>
      </div>
    </>
  );
}
