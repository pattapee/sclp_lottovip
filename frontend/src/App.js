import React from "react";
import { Routes, Route } from "react-router-dom";

import LoginPage from "./pages/Login";
import NotFound from "./pages/Notfound";
import DashboardPage from "./pages/Dashboard";
import LottoPage from "./pages/Lotto";
import LottoCreatePage from "./pages/Lotto/create";
import LottoUpdatePage from "./pages/Lotto/update";
import SettingPage from "./pages/Setting";
import UsersPage from "./pages/Users";
import UserCreatePage from "./pages/Users/create";
import UserUpdatePage from "./pages/Users/update";
import PredictMookPage from "./pages/Predict";
import PredictMookCreatePage from "./pages/Predict/create";
import PredictMookUpdatePage from "./pages/Predict/update";

import AuthenAdminPage from "./pages/Layout/authenadmin";
import { ProtectedRoute } from "./services/protectedRoute";

export default function App() {
  return (
    <Routes>
      {/* Route Non Login */}
      <Route>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/" element={<LoginPage />} />
        <Route path="*" element={<NotFound />} />
      </Route>

      {/* Route Dashboard  */}
      <Route
        path="/dashboard"
        render
        element={
          <ProtectedRoute>
            <AuthenAdminPage>
              <DashboardPage />
            </AuthenAdminPage>
          </ProtectedRoute>
        }
      />

      <Route path="/lotto">
        <Route
          path="list/:category"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <LottoPage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path=":id"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <LottoUpdatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path="create"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <LottoCreatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path="predict/adminmook"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <PredictMookPage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path="predict/adminmook/create"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <PredictMookCreatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path="predict/adminmook/:id"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <PredictMookUpdatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
      </Route>

      {/* Route Users */}
      <Route path="/user">
        <Route
          path="list"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <UsersPage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path="create"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <UserCreatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
        <Route
          path=":id"
          element={
            <ProtectedRoute>
              <AuthenAdminPage>
                <UserUpdatePage />
              </AuthenAdminPage>
            </ProtectedRoute>
          }
        />
      </Route>
      {/* Route Setting */}
      <Route
        path="/setting"
        element={
          <ProtectedRoute>
            <AuthenAdminPage>
              <SettingPage />
            </AuthenAdminPage>
          </ProtectedRoute>
        }
      />
    </Routes>
  );
}
